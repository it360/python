#!/usr/bin/python
import os
import logging
import urllib2
import subprocess

def logging_setup():
    log_path = '/usr/local/it360/logs'
    log_name = 'dockutil_installer.log'
    log_file = os.path.join(log_path, log_name)
    
    if not os.path.isdir(log_path):
        os.makedirs(log_path)
    
    logging.basicConfig(filename=log_file, level=logging.DEBUG,\
            format='%(asctime)s - %(levelname)s -  %(message)s',\
            datefmt='%Y-%m-%d %I:%M:%S %p')
            
def logger(log):
    # levels: print, log, verbose
    log_level = "verbose"
    if log_level == "print":
        print(log)
    elif log_level == "log":
        logging.debug(log)
    elif log_level == "verbose":
        print(log)
        logging.debug(log)

def termy(cmd):
    out = ""
    err = ""
    
    task = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = task.communicate()
    
    if out != "":
        logger("Subprocessing output: {0}".format(out.strip()))
    if err != "":
        logger("Subprocessing errors: {0}".format(err.strip()))

def folder_checks():
    bin_path = "/usr/local/it360/bin/"
    
    if not os.path.isdir(bin_path):
        logger("The bin folder doesn't exist.")
        logger("Creating {0} ...\n".format(bin_path))
        try:
            os.makedirs(bin_path)
        except OSError:
            logger("Permission denied, please run as root, exiting...")
            exit(1)
        except:
            logger("Error creating the it360/bin folder, exiting...")
            exit(1)
        logger("Created {0}".format(bin_path))

def dockutil_install():
    dockutil = '/usr/local/it360/bin/dockutil'
    
    url = "https://raw.githubusercontent.com/kcrawford/dockutil/master/scripts/dockutil"
    page = urllib2.urlopen(url)
    data = page.read()
    page.close()
    
    try:
        file = open(dockutil, 'w')
        file.write(data)
        file.close()
    except IOError:
        logger("Permission denied, please run as root, exiting...")
        exit(1)
    except:
        logger("Error saving dockutil, exiting...")
        exit(1)

def permission_fix():
    dockutil = '/usr/local/it360/bin/dockutil'
    cmds = []
    cmds.append(['chown', '-R', 'root:staff', '/usr/local/it360'])
    cmds.append(['chmod', '-R', '755', '/usr/local/it360/'])
    cmds.append(['chmod', '-R', '774', '/usr/local/it360/logs'])
    cmds.append(["chmod", "+x", dockutil])
    
    for cmd in cmds:
        termy(cmd)


logging_setup()
folder_checks()
dockutil_install()
permission_fix()
