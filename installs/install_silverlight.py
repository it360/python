#!/usr/bin/env python

import urllib2, os, subprocess, shutil, time, re, sys, getpass
from sys import argv, exit
from distutils.version import LooseVersion

script, log_file, mode = argv

the_user = getpass.getuser()
if the_user != "root":
    print "[Exiting], please run as root."
    exit(1)

class install(object):
    def __init__(self):
        self.app_name = "Silverlight"
        self.url = "http://www.microsoft.com/getsilverlight/handlers/getsilverlight.ashx"
        self.vers_url = "https://www.macupdate.com/app/mac/26623/silverlight"
        self.dmg_path = "/tmp/silverlight.dmg"
        self.mnt_path = "/Volumes/Silverlight"
        self.inst_base = "/Library/Internet Plug-Ins/Silverlight.plugin"
        self.pkg = '/Volumes/Silverlight/Silverlight.pkg'
        self.inst_inf = self.inst_base+"/Contents/Info.plist"
        self.user_agent = [('User-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/536.28.10 (KHTML, like Gecko) Version/6.0.3 Safari/536.28.10')]
    
    def check_inst(self):
        return os.path.isdir(self.inst_base)
    
    def convert_plist(self, fmt, path):
        cmd = ["plutil", "-convert", fmt, path]
        subprocess.call(cmd)
    
    def check_version(self, latest_version):
        self.convert_plist("xml1", self.inst_inf)
        a = open(self.inst_inf, "r")
        lines = a.readlines()
        r = re.compile(r'CFBundleVersion')
        for i in range(len(lines)):
            if r.search(lines[i]):
                c = lines[max(0, i+1)]
        d = c.split('<string>')
        e = ''.join(d[1])
        f = e.split('</string>')
        g = f[0]
        self.convert_plist("binary1", self.inst_inf)
        return LooseVersion(g) >= LooseVersion(latest_version), g
    
    def download(self):
        opener = urllib2.build_opener()
        opener.addheaders = self.user_agent
        f = opener.open(self.url)
        local_file = open(self.dmg_path, "w") # open the file to write to
        local_file.write(f.read()) # write the download to the local file
        local_file.close() # close the file
        a = os.path.isfile(self.dmg_path)
        if a == True:
            return "%s was downloaded.\n" % self.app_name
        elif a == False:
            print "Something went wrong and %s wasn't downloaded, exiting." % self.app_name
            exit(1)
        else:
            print "[Error] at download function, exiting."
            exit(1)
    
    def mount(self):
        cmd = ['/usr/bin/hdiutil', 'attach', '-nobrowse', self.dmg_path]
        subprocess.Popen(cmd, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
        return self.check()
    
    def check(self):
        a = False
        b = 0
        while (a == False) and (b < 60):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print "..."
            b += 1
        if a == False:
            print "%s wasn't mounted." % self.app_name
            exit(1)
        elif a == True:
            return "%s was mounted.\n" % self.app_name
        else:
            print "[Error] at mount/check functions, exiting."
            exit(1)
    
    def inst(self, latest_version):
        cmd = ['/usr/sbin/installer', '-pkg', self.pkg, '-target', '/']
        subprocess.call(cmd)#, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        a = os.path.isdir(self.inst_base)
        if a == True:
            vers = self.check_version(latest_version)
            if vers[0] == True:
                notice = "%(x)s version %(y)s was installed.\n" % {"x" : self.app_name, "y" : vers[1]}
            elif vers[0] == False:
                notice = "%s is installed but is still not up-to-date. current version: %s" % self.app_name, vers
            else:
                print "[Error] at install success version function/statement, exiting."
                print a
                print vers
                exit(1)
            return notice, vers
        elif a == False:
            return "[Error] %s wasn't installed, exiting." % self.app_name
        else:
            print "[Error] at install function, exiting."
            exit(1)
    
    def unmount(self):
        cmd = ['/usr/bin/hdiutil', 'detach', '-force', self.mnt_path]
        subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        a = os.path.isdir(self.mnt_path)
        b = 0
        while (a == True) and (b < 30):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print "..."
            b += 1
        if a == False:
            return "Unmounted %s.\n" % self.app_name
        elif a == True:
            return  "[Error] couldn't unmount %s, exiting." % self.app_name
        else:
            print "[Error] at unmount function, exiting."
            exit(1)
    
    def clean(self):
        os.unlink(self.dmg_path)
        aa = os.path.isfile(self.dmg_path)
        bb = 0
        while (aa == True) and (bb < 30):
            time.sleep(1)
            aa = os.path.isdir(self.mnt_path)
            print "..."
            bb += 1
        if bb == False:
            y = "%s dmg has been deleted.\n" % self.app_name
            return y
        elif bb == True:
            return "[Error] couldn't delete the %s DMG, exiting." % self.app_name
        else:
            return "[Error] at clean function, exiting."
            exit(1)
            
    def verify(self, latest_version):
        verify_inst = os.path.isdir(self.inst_base)
        verify_vers = self.check_version(latest_version)
        verify_unmount = os.path.isdir(self.mnt_path)
        if verify_unmount == True:
            verify_unmount = False
        elif verify_unmount == False:
            verify_unmount = True
        else:
            print "[Error] at verify_unmount, exiting."
            exit(1)
        # check that the dmg was deleted
        verify_rmdmg = os.path.isfile(self.dmg_path)
        if verify_rmdmg == True:
            verify_rmdmg = False
        elif verify_rmdmg == False:
            verify_rmdmg = True
        else:
            print "[Error] at verify_rmdmg, exiting."
            exit(1)
        # report
        verified = verify_inst, verify_vers, verify_unmount, verify_rmdmg
        if False in verified:
            a = "[Error] Something couldn't complete:\n"
            b = "Install: %s\n" % verify_inst
            c = "Updated: %(x)s %(y)s\n" % {"x":verify_vers[0], "y":verify_vers[1]}
            d = "Unmounted: %s\n" % verify_unmount
            e = "Removed DMG: %s\n" % verify_rmdmg
            return a, b, c, d, e
        elif False not in verified:
            a = "[Success], %s has been installed.\n" % self.app_name
            b = "Install: %s\n" % verify_inst
            c = "Updated: %s\n" % verify_vers[0]
            d = "Unmounted: %s\n" % verify_unmount
            e = "Removed DMG: %s\n" % verify_rmdmg
            return a, b, c, d, e
        else:
            print "[Error] at verification return, exiting."
            exit(1)
        
    def the_log(self, log_file, log_out):
        for i in log_out:
            print i
        a = open(log_file, "a")
        for i in log_out:
            a.write(i)
        a.close()
        print "Local log file found at: %s" % log_file

def get_latest_version():
    sock = urllib2.urlopen(the_run.vers_url)
    html_read = sock.read()
    sock.close()
    m = re.search(r'<meta name="twitter:data2" content="(.*)">', html_read)
    return m.group(1)

def inst_proc(latest_version, log_file):
    print the_run.download()
    print the_run.mount()
    print the_run.inst(latest_version)
    print the_run.unmount()
    print the_run.clean()
    log_out = the_run.verify(latest_version)
    the_run.the_log(log_file, log_out)

the_run = install()
latest_version = get_latest_version()
var = the_run.check_inst()

if mode == "install":
    if var == True:
        ver, g = the_run.check_version(latest_version)
        if ver == True:
            print "%s is already up-to-date, exiting." % the_run.app_name
            print g
        elif ver == False:
            print "%s needs to update, starting the updater." % the_run.app_name
            print g+" > "+latest_version
            inst_proc(latest_version, log_file)
    elif var == False:
        print "%s is not installed, starting the installer." % the_run.app_name
        inst_proc(latest_version, log_file)
    else:
        print("[Error], at mode install, exiting...")
        exit(1)
elif mode == "update":
    if var == True:
        ver, g = the_run.check_version(latest_version)
        if ver == True:
            print "%s is already up-to-date, exiting." % the_run.app_name
            print g
        elif ver == False:
            print "%s needs to update, starting the updater." % the_run.app_name
            print g+" > "+latest_version
            inst_proc(latest_version, log_file)
    elif var == False:
        print "%s is not installed." % the_run.app_name
        print("If you would like to install %s please use the install option.") % the_run.app_name
        print("Exiting...")
        exit(0)
    else:
        print("[Error], at mode install, exiting...")
        exit(1)
    
# sudo python Desktop/inst_flash.py /Users/Shared/.labtech/logs/`date +%m-%d-%Y_%H%M%S`.txt

# parse macupdate for the version
# https://www.macupdate.com/app/mac/26623/silverlight

# two variants
# install and update
# update gets ran every week
# install is for, well, installing
# while updating check if it's installed
# if it's not installed, exit