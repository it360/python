#!/usr/bin/python
import os
import glob
import time
import shutil
import urllib2
import subprocess

def termy(cmd):
    task = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = task.communicate()
    return(out, err)

def console_user():
    cmd = '/usr/bin/stat -f "%Su" /dev/console'.split(' ')
    out, err = termy(cmd)
    user = out.strip()
    return(user.replace('"', ''))

def get_agents():
    root_cmd = ['launchctl', 'list']

    root_out, root_err = termy(root_cmd)

    root_data = root_out.split('\n')
    for _ in root_data:
        if 'screenconnect' in _:
            root_agent = _.split('\t')[-1]
    
    return(root_agent)
            
def remove_agents():
    root_agent = get_agents()
    root_cmd = ['launchctl', 'remove', root_agent]
    root_out = termy(root_cmd)
    
def remove_files():
    sc_app = glob.glob('/opt/screenconnect-*')
    sc_la = glob.glob('/Library/LaunchAgents/screenconnect-*')
    sc_ld = glob.glob('/Library/LaunchDaemons/screenconnect-*')
    for _ in sc_app:
        shutil.rmtree(_)
    for _ in sc_la:
        os.unlink(_)
    for _ in sc_ld:
        os.unlink(_)
    
def unload_user_agent(unload_script):
    script = '/Users/Shared/.labtech/auto_run/unloader.sh'
    file = open(script, 'w')
    file.write(unload_script)
    file.close()
    cmd = ['chown', console_user(), script]
    cmd2 = ['chmod', '+x', script]
    termy(cmd)
    termy(cmd2)

def download_sc(sc_installer):
    url = 'http://w.bryan.nu/sim/ScreenConnect.ClientSetup.pkg'
    page = urllib2.urlopen(url)
    file = open(sc_installer, 'w')
    file.write(page.read())
    file.close()
    
def install_sc():
    sc_installer = '/private/tmp/ScreenConnect.ClientSetup.pkg'
    download_sc(sc_installer)
    cmd = ['installer', '-pkg', sc_installer, '-target', '/']
    if os.path.exists(sc_installer):
        termy(cmd)
        
def load_user_agent(load_script):
    script = '/Users/Shared/.labtech/auto_run/loader.sh'
    time.sleep(2)
    la = glob.glob('/Library/LaunchAgents/screenconnect-*-onlogin*')[0]
    file = open(script, 'w')
    file.write(load_script.format(la))
    file.close()
    cmd = ['chown', console_user(), script]
    cmd2 = ['chmod', '+x', script]
    termy(cmd)
    termy(cmd2)

unload_script = """#!/bin/bash
sc=$(launchctl list | awk '/screenconnect/ {print $3}')
launchctl remove "$sc"
"""

load_script = """#!/bin/bash
launchctl load {0}
"""

        
try:
    remove_agents()
except:
    pass
try:
    remove_files()
except:
    pass
if os.path.exists('/Users/Shared/.labtech/auto_run/'):
    unload_user_agent(unload_script)
install_sc()
load_user_agent(load_script)