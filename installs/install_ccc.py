#!/usr/bin/env python

import urllib2, os, subprocess, time, re, getpass
from sys import argv, exit
from distutils.version import LooseVersion

script, log_file = argv

the_user = getpass.getuser()
if the_user != "root":
    print "[Exiting], please run as root."
    exit(1)

class install(object):
    def __init__(self):
        self.app_name = "Carbon Copy Cloner"
        self.the_app = self.app_name + ".app"
        self.info_file = "/Applications/%s/Contents/Info.plist" % self.the_app
        self.latest_vers = "4.1.13"
        self.url = "http://files.bombich.com/ccc-4.1.13.4496.zip"
        self.zip_path = "/tmp/ccc.zip"
        self.unzip_cmd = ['unzip', self.zip_path, '-d', '/Applications/']
        self.dst = "/Applications/Carbon Copy Cloner.app"
    
    def check_inst(self):
        return os.path.isdir(self.dst)
    
    def check_version(self):
        a = open(self.info_file, "r")
        lines = a.readlines()
        r = re.compile(r'CFBundleShortVersionString')
        for i in range(len(lines)):
            if r.search(lines[i]):
                c = lines[max(0, i+1)]
        d = c.split('<string>')
        e = ''.join(d[1])
        f = e.split('</string>')
        g = f[0]
        return LooseVersion(g) >= LooseVersion(self.latest_vers), g
    
    def download(self):
        print "Downloading %s..." % self.app_name
        f = urllib2.urlopen(self.url) # open the url
        local_file = open(self.zip_path, "w") # open the file to write to
        local_file.write(f.read()) # write the download to the local file
        local_file.close() # close the file
        a = os.path.isfile(self.zip_path)
        if a == True:
            print "%s was downloaded." % self.app_name
            return "%s was downloaded.\n" % self.app_name
        elif a == False:
            print "[Error], %s couldn't be downloaded, exiting." % self.app_name
            exit(1)
        else:
            print "[Error], exiting."
            exit(1)
    
    def unzip(self):
        print "Unzipping and installing %s" % self.app_name
        with open(os.devnull, "w") as f:
            subprocess.call(self.unzip_cmd, stdout=f)
        a = os.path.isdir(self.dst)
        if a == True:
            print  "%s was unzipped and installed." % self.app_name
            return "%s was unzipped and installed.\n" % self.app_name
        elif a == False:
            print "[Error], %s was't installed." % self.app_name
            return "[Error], %s wasn't installed.\n" % self.app_name
        else:
            print "[Error], exiting."
            exit(1)
        
    def cleanup(self):
        os.unlink(self.zip_path)
        aa = os.path.isfile(self.zip_path)
        bb = 0
        while (aa == True) and (bb < 30):
            time.sleep(1)
            aa = os.path.isdir(self.zip_path)
            print "..."
            bb += 1
        if bb == False:
            print "The %s zip was deleted." % self.app_name
            return "The %s zip was deleted.\n" % self.app_name
        elif bb == True:
            return "[Error], couldn't delete the %s zip." % self.app_name
        else:
            print "[Error], exiting."
            exit(1)
    
    def the_log(self, log_file, e):
        a = open(log_file, "a")
        for i in e:
            a.write(i)
        a.close()
        print e[3]

def install_process(a):
    b = a.download()
    c = a.unzip()
    d = a.cleanup()
    e = [b, c, d]
    for h in e:
        if "[Error]" not in h:
            f = "[Success], %s was installed." % a.app_name
            e = [b, c, d, f]
        else:
            f = "[Error], something didn't complete properly. Please check the logs."
            e = [b, c, d, f]
            break
    a.the_log(log_file, e)
    exit(0)

a = install()
cc = a.check_inst()
if cc == True:
    aa, bb = a.check_version()
elif cc == False:
    install_process(a)
else:
    a.the_log(log_file, "error!")
    exit(1)
if aa == True:
    g = "[Success], up-to-date\n%s\n" % bb
    a.the_log(log_file, g)
    print "[Success], %(z)s is up-to-date (%(y)s)" % {'z':a.app_name, 'y':bb}
    exit(0)
elif aa == False:
    install_process(a)
else:
    a.the_log(log_file, "error!")
    exit(1)