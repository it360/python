#!/usr/bin/env python

import urllib2, os, subprocess, shutil, time, re, sys, getpass
from sys import argv, exit
from distutils.version import LooseVersion

script, log_file = argv

the_user = getpass.getuser()
if the_user != "root":
    print "Please run as root, exiting."
    exit(1)

class install(object):
    def __init__(self):
        self.app_name = "forticlientsslvpn"
        self.latest_version = "4.4.2297"
        self.url = "http://w.somethingtechnical.net/forticlientsslvpn_macosx_4.4.2297.dmg"
        self.dmg_path = "/private/tmp/forticlientsslvpn_macosx_4.4.2297.dmg"
        self.mnt_path = "/Volumes/forticlientsslvpn.pkg"
        self.inst_base = "/Applications/forticlientsslvpn.app"
        self.pkg = '/Volumes/forticlientsslvpn.pkg/forticlientsslvpn.pkg'
        self.inst_inf = self.inst_base+"/Contents/Info.plist"
        self.user_agent = [('User-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/536.28.10 (KHTML, like Gecko) Version/6.0.3 Safari/536.28.10')]
        self.install_receipt = '/Users/Shared/.labtech/receipts/sslvpn_4.4.2297.inst'
    
    def check_inst(self):
        if os.path.isfile(self.install_receipt) == True:
            if os.path.isdir(self.inst_base) == True:
                print("The latest version is installed, exiting...")
            exit()
        return os.path.isdir(self.inst_base)
    
    def convert_plist(self, fmt, path):
        cmd = ["plutil", "-convert", fmt, path]
        subprocess.call(cmd)
    
    def check_version(self):
        file = open(self.inst_inf, "r")
        lines = file.readlines()
        file.close()
        
        count = 0
        loop = True
        while loop == True:
            for line in lines:
                count += 1
                if "CFBundleVersion" in line:
                    version_line = count
                    loop = False
                    break
                    
        version = lines[version_line]\
            .replace("<string>", "")\
            .replace("</string>", "")\
            .strip()
            
        print(version)
        return(LooseVersion(version) >= LooseVersion(self.latest_version), version)
    
    def download(self):
        opener = urllib2.build_opener()
        opener.addheaders = self.user_agent
        f = opener.open(self.url)
        local_file = open(self.dmg_path, "w")
        local_file.write(f.read())
        local_file.close()
        a = os.path.isfile(self.dmg_path)
        if a == True:
            return "%s was downloaded.\n" % self.app_name
        elif a == False:
            print "Something went wrong and %s wasn't downloaded, exiting." % self.app_name
            exit(1)
        else:
            print "[Error] at download function, exiting."
            exit(1)
    
    def mount(self):
        cmd = ['/usr/bin/hdiutil', 'attach', '-nobrowse', self.dmg_path]
        subprocess.Popen(cmd, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
        return self.check()
    
    def check(self):
        a = False
        b = 0
        while (a == False) and (b < 60):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print "..."
            b += 1
        if a == False:
            print "Something went wrong and %s still hasn't mounted." % self.app_name
            exit(1)
        elif a == True:
            return "%s was mounted.\n" % self.app_name
        else:
            print "[Error] at mount/check functions, exiting."
            exit(1)
    
    def inst(self):
        cmd = ['/usr/sbin/installer', '-pkg', self.pkg, '-target', '/']
        subprocess.call(cmd)
        a = os.path.isdir(self.inst_base)
        self.create_receipt()
        if a == True:
            vers = self.check_version()
            if vers[0] == True:
                notice = "%(x)s version %(y)s was installed.\n" % {"x" : self.app_name, "y" : vers[1]}
            elif vers[0] == False:
                notice = "%s is installed but is still not up-to-date. current version: %s" % (self.app_name, vers)
            else:
                print "[Error] at install success version function/statement, exiting."
                print a
                print vers
                exit(1)
            return notice, vers
        elif a == False:
            return "[Error] %s wasn't installed, exiting." % self.app_name
            exit(1)
        else:
            print "[Error] at install function, exiting."
            exit(1)
    
    def unmount(self):
        cmd = ['/usr/bin/hdiutil', 'detach', '-force', self.mnt_path]
        subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        a = os.path.isdir(self.mnt_path)
        b = 0
        while (a == True) and (b < 30):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print "..."
            b += 1
        if a == False:
            return "Unmounted %s.\n" % self.app_name
        elif a == True:
            print  "[Error] couldn't unmount %s, exiting." % self.app_name
            exit(1)
        else:
            print "[Error] at unmount function, exiting."
            exit(1)
    
    def clean(self):
        os.unlink(self.dmg_path)
        aa = os.path.isfile(self.dmg_path)
        bb = 0
        while (aa == True) and (bb < 30):
            time.sleep(1)
            aa = os.path.isdir(self.mnt_path)
            print "..."
            bb += 1
        if bb == False:
            y = "%s dmg has been deleted.\n" % self.app_name
            return y
        elif bb == True:
            return "[Error] couldn't delete the %s DMG, exiting." % self.app_name
            exit(1)
        else:
            return "[Error] at clean function, exiting."
            exit(1)
            
    def create_receipt(self):
        file = open(self.install_receipt, 'w')
        file.write('installed')
        file.close()
            
    def verify(self):
        # check if flash is installed
        verify_inst = os.path.isdir(self.inst_base)
        # check what version
        verify_vers = self.check_version()
        # check that the dmg is unmounted
        verify_unmount = os.path.isdir(self.mnt_path)
        if verify_unmount == True:
            verify_unmount = False
        elif verify_unmount == False:
            verify_unmount = True
        else:
            print "[Error] at verify_unmount, exiting."
            exit(1)
        # check that the dmg was deleted
        verify_rmdmg = os.path.isfile(self.dmg_path)
        if verify_rmdmg == True:
            verify_rmdmg = False
        elif verify_rmdmg == False:
            verify_rmdmg = True
        else:
            print "[Error] at verify_rmdmg, exiting."
            exit(1)
        # report
        verified = verify_inst, verify_vers, verify_unmount, verify_rmdmg
        if False in verified:
            a = "[Error] Something couldn't complete:\n"
            b = "Install: %s\n" % verify_inst
            c = "Updated: %(x)s %(y)s\n" % {"x":verify_vers[0], "y":verify_vers[1]}
            d = "Unmounted: %s\n" % verify_unmount
            e = "Removed DMG: %s\n" % verify_rmdmg
            return a, b, c, d, e
        elif False not in verified:
            a = "[Success], %s has been installed.\n" % self.app_name
            b = "Install: %s\n" % verify_inst
            c = "Updated: %s\n" % verify_vers[0]
            d = "Unmounted: %s\n" % verify_unmount
            e = "Removed DMG: %s\n" % verify_rmdmg
            return a, b, c, d, e
        else:
            print "[Error] at verification return, exiting."
            exit(1)
        
    def the_log(self, log_file, log_out):
        for i in log_out:
            print i
        a = open(log_file, "a")
        for i in log_out:
            a.write(i)
        a.close()
        print "Local log file found at: %s" % log_file



the_run = install()
var = the_run.check_inst()
if var == True:
    # check version
    ver, g = the_run.check_version()
    if ver == True:
        print "%s is already up-to-date, exiting." % the_run.app_name
        print g
    elif ver == False:
        print "%s needs to update, starting the updater." % the_run.app_name
        print g+" > "+the_run.latest_version
        print the_run.download()
        print the_run.mount()
        print the_run.inst()
        print the_run.unmount()
        print the_run.clean()
        log_out = the_run.verify()
        the_run.the_log(log_file, log_out)
elif var == False:
    print "%s is not installed, starting the installer." % the_run.app_name
    print the_run.download()
    print the_run.mount()
    print the_run.inst()
    print the_run.unmount()
    print the_run.clean()
    log_out = the_run.verify()
    the_run.the_log(log_file, log_out)
else:
    print "?"
    
# sudo python Desktop/inst_flash.py /Users/Shared/.labtech/logs/`date +%m-%d-%Y_%H%M%S`.txt