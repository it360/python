#!/usr/bin/env python

import urllib2, os, time, re, getpass, tarfile
from sys import argv, exit
from distutils.version import LooseVersion

script, log_file = argv

the_user = getpass.getuser()
if the_user != "root":
    print "[Exiting], please run as root."
    exit(1)

class install(object):
    def __init__(self):
        self.app_name = "OmniGraffle Professional 5"
        self.the_app = self.app_name + ".app"
        self.info_file = "/Applications/%s/Contents/Info.plist" % self.the_app
        self.url = "http://www.omnigroup.com/ftp/pub/software/MacOSX/10.6/OmniGrafflePro-5.4.4.tbz2"
        self.vers_url = "http://www.macupdate.com/app/mac/11441/omnigraffle-pro"
        self.latest_vers = "5.4.4"
        self.tar_path = "/tmp/OmniGrafflePro.tbz2"
        self.dst = "/Applications/"
        self.app_path = self.dst+self.the_app
    
    def get_latest_version(self):
        print('Getting the latest version number...')
        sock = urllib2.urlopen(self.vers_url)
        html_read = sock.read()
        sock.close()
        m = re.search(r'<meta name="twitter:data2" content="(.*)">', html_read)
        self.latest_vers = m.group(1)
        #self.url = self.url % {'url' : self.latest_vers}
        #self.src = self.src % self.latest_vers
        #self.mnt_path = self.mnt_path % self.latest_vers
    
    def check_inst(self):
        return os.path.isdir(self.app_path)
    
    def check_version(self):
        a = open(self.info_file, "r")
        lines = a.readlines()
        r = re.compile(r'CFBundleShortVersionString')
        for i in range(len(lines)):
            if r.search(lines[i]):
                c = lines[max(0, i+1)]
        d = c.split('<string>')
        e = ''.join(d[1])
        f = e.split('</string>')
        g = f[0]
        return LooseVersion(g) >= LooseVersion(self.latest_vers), g
    
    def download(self):
        print "Downloading %s..." % self.app_name
        f = urllib2.urlopen(self.url) # open the url
        local_file = open(self.tar_path, "w") # open the file to write to
        local_file.write(f.read()) # write the download to the local file
        local_file.close() # close the file
        a = os.path.isfile(self.tar_path)
        if a == True:
            print "%s was downloaded." % self.app_name
            return "%s was downloaded.\n" % self.app_name
        elif a == False:
            print "[Error], %s couldn't be downloaded, exiting." % self.app_name
            exit(1)
        else:
            print "[Error], exiting."
            exit(1)
        
    def untar(self):
        print("Installing %s..." % self.app_name)
        tar = tarfile.open(self.tar_path)
        tar.extractall(self.dst)
        tar.close()
        installed = os.path.isdir(self.app_path)
        if installed == True:
            print("%s was installed." % self.app_name)
            return("%s was installed.\n" % self.app_name)
        elif installed == False:
            return("[Error] %s wasn't installed.\n" % self.app_name)
        else:
            print("[Error] at untar.")
            return("[Error] at untar.\n")
        
    def cleanup(self):
        print("Deleting %s tar file..." % self.app_name)
        os.unlink(self.tar_path)
        file = os.path.isfile(self.tar_path)
        runs = 0
        while (file == True) and (runs < 30):
            time.sleep(1)
            file = os.path.isdir(self.tar_path)
            print("...")
            runs += 1
        if file == False:
            print("%s tar file was deleted." % self.app_name)
            return("%s tar file was deleted.\n" % self.app_name)
        elif bb == True:
            print("[Error] couldn't delete the %s tar file." % self.app_name)
            return("[Error] couldn't delete the %s tar file.\n" % self.app_name)
        else:
            print("[Error] while trying to delete the %s tar file." % self.app_name)
            return("[Error] while trying to delete the %s tar file.\n" % self.app_name)
    
    def the_log(self, log_file, g):
        a = open(log_file, "a")
        for i in g:
            a.write(i)
        a.close()

def install_process(a):
    download_results = a.download()
    untar_results = a.untar()
    cleanup_results = a.cleanup()
    log_list = [download_results, untar_results, cleanup_results]
    for _ in log_list:
        if "[Error]" not in _:
            the_success = "[Success] %s was installed.\n" % a.app_name
            updated_log_list = [download_results, untar_results, cleanup_results, the_success]
        else:
            the_success = "[Error] something didn't complete properly. Please check the logs.\n"
            updated_log_list = [download_results, untar_results, cleanup_results, the_success]
            break
    print(the_success)
    a.the_log(log_file, updated_log_list)
    exit(0)

a = install()
#a.get_latest_version()
cc = a.check_inst()
if cc == True:
    aa, bb = a.check_version()
elif cc == False:
    install_process(a)
else:
    a.the_log(log_file, "error!")
    exit(1)

if aa == True:
    g = "[Success] up-to-date\n%s\n" % bb
    a.the_log(log_file, g)
    print("[Success] %(z)s is up-to-date (%(y)s)" % {'z':a.app_name, 'y':bb})
    exit(0)
elif aa == False:
    install_process(a)
else:
    a.the_log(log_file, "error!")
    exit(1)
    
# taking the base of install_dmg and replacing what i need to with the zip bits
# then changing the zip bits to work with a .tbz2 archive