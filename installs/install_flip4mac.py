#!/usr/bin/env python

import urllib2, os, subprocess, shutil, time, re, sys, getpass
from sys import argv, exit
from distutils.version import LooseVersion

script, log_file = argv

the_user = getpass.getuser()
if the_user != "root":
    print "Please run as root, exiting."
    exit(1)

class install(object):
    def __init__(self):
        self.app_name = "Flip4Mac"
        self.url = "https://www.macupdate.com/download/17787/Flip4Mac%20%(url)s.dmg"
        self.vers_url = "https://www.macupdate.com/app/mac/17787/flip4mac"
        self.dmg_path = "/tmp/Flip4Mac.dmg"
        self.mnt_path = "/Volumes/Flip4Mac/"
        self.inst_base = "/Applications/Flip Player.app"
        self.pkg = '/Volumes/Flip4Mac/Flip4Mac.pkg'
        self.inst_inf = self.inst_base+"/Contents/Info.plist"
        self.user_agent = [('User-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/536.28.10 (KHTML, like Gecko) Version/6.0.3 Safari/536.28.10')]
        
    def get_latest_version(self):
        print('Getting the latest version number...')
        sock = urllib2.urlopen(self.vers_url)
        html_read = sock.read()
        sock.close()
        m = re.search(r'<meta name="twitter:data2" content="(.*)">', html_read)
        self.latest_vers = m.group(1)
        self.url = self.url % {'url':self.latest_vers}
        print(self.latest_vers)
        
    def check_inst(self):
        return os.path.isdir(self.inst_base)
    
    def convert_plist(self, fmt, path):
        cmd = ["plutil", "-convert", fmt, path]
        subprocess.call(cmd)
    
    def check_version(self):
        self.convert_plist("xml1", self.inst_inf)
        a = open(self.inst_inf, "r")
        lines = a.readlines()
        r = re.compile(r'CFBundleVersion')
        for i in range(len(lines)):
            if r.search(lines[i]):
                c = lines[max(0, i+1)]
        d = c.split('<string>')
        e = ''.join(d[1])
        f = e.split('</string>')
        g = f[0]
        self.convert_plist("binary1", self.inst_inf)
        if g == "":
            g = "0"
            return False, g
        return LooseVersion(g) >= LooseVersion(self.latest_vers), g
    
    def download(self):
        print('Downloading %s...') % self.app_name
        opener = urllib2.build_opener()
        opener.addheaders = self.user_agent
        f = opener.open(self.url)
        #f = urllib2.urlopen(self.url) # open the url
        local_file = open(self.dmg_path, "w") # open the file to write to
        local_file.write(f.read()) # write the download to the local file
        local_file.close() # close the file
        a = os.path.isfile(self.dmg_path)
        if a == True:
            print('%s was downloaded.') % self.app_name
            return "%s was downloaded.\n" % self.app_name
        elif a == False:
            print("%s wasn't downloaded, exiting." % self.app_name)
            exit(1)
        else:
            print("[Error] at download function, exiting.")
            exit(1)
    
    def mount(self):
        print('Mounting %s...') % self.app_name
        cmd = ['/usr/bin/hdiutil', 'attach', '-nobrowse', self.dmg_path]
        subprocess.Popen(cmd, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
        return self.check()
    
    def check(self):
        a = False
        b = 0
        while (a == False) and (b < 60):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print("...")
            b += 1
        if a == False:
            print("[Error], %s wasn't mounted." % self.app_name)
            exit(1)
        elif a == True:
            print('%s was mounted.') % self.app_name
            return "%s was mounted.\n" % self.app_name
        else:
            print("[Error] at mount/check functions, exiting.")
            exit(1)
    
    def inst(self):
        print("Installing %s..." % self.app_name)
        cmd = ['/usr/sbin/installer', '-pkg', self.pkg, '-target', '/']
        subprocess.call(cmd)
        a = os.path.isdir(self.inst_base)
        if a == True:
            vers = self.check_version()
            if vers[0] == True:
                print("%s was installed." % self.app_name)
                notice = "%(x)s version %(y)s was installed.\n" % {"x" : self.app_name, "y" : vers[1]}
                return(notice)
            elif vers[0] == False:
                notice = "%s is installed but is still not up-to-date. current version: %s\n" % self.app_name, vers
                print(notice)
                return(notice)
            else:
                err_txt = "[Error] at install success version function/statement, exiting.\n"
                print(err_txt)
                print(a)
                print(vers)
                return(err_txt, vers)
        elif a == False:
            print("[Error] %s wasn't installed." % self.app_name)
            return("[Error] %s wasn't installed.\n" % self.app_name)
        else:
            print("[Error] at install function, exiting.")
            return("[Error] at install function, exiting.\n")
    
    def unmount(self):
        print('Unmounting %s DMG...' % self.app_name)
        cmd = ['/usr/bin/hdiutil', 'detach', '-force', self.mnt_path]
        subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        a = os.path.isdir(self.mnt_path)
        b = 0
        while (a == True) and (b < 30):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print("...")
            b += 1
        if a == False:
            print('%s was unmounted.' % self.app_name)
            return "%s was unmounted.\n" % self.app_name
        elif a == True:
            print("[Error] couldn't unmount %s, exiting." % self.app_name)
            exit(1)
        else:
            print("[Error] at unmount function, exiting.")
            exit(1)
    
    def clean(self):
        print('Deleting %s DMG...' % self.app_name)
        os.unlink(self.dmg_path)
        aa = os.path.isfile(self.dmg_path)
        bb = 0
        while (aa == True) and (bb < 30):
            time.sleep(1)
            aa = os.path.isdir(self.mnt_path)
            print("...")
            bb += 1
        if bb == False:
            y = "%s dmg has been deleted.\n" % self.app_name
            print(y)
            return(y)
        elif bb == True:
            print("[Error] couldn't delete the %s DMG, exiting." % self.app_name)
            exit(1)
        else:
            print("[Error] at clean function, exiting.")
            exit(1)
            
    def the_log(self, log_file, log_out):
        #for i in log_out:
        #    print i
        a = open(log_file, "a")
        for i in log_out:
            a.write(i)
        a.close()
        print "The local log file found at: %s" % log_file

def inst_proc(the_run):
    a = the_run.download()
    b = the_run.mount()
    c = the_run.inst()
    d = the_run.unmount()
    e = the_run.clean()
    f = [a, b, c, d, e]
    for _ in f:
        if "[Error]" not in _:
            g = "[Success] %s was installed.\n" % the_run.app_name
            h = [a, b, c, d, e, g]
        else:
            g = "[Error] An Error was found, please check the logs.\n"
            h = [a, b, c, d, e, g]
            break
    print(g)
    the_run.the_log(log_file, h)

the_run = install()
var = the_run.check_inst()
the_run.get_latest_version()
if var == True:
    ver, g = the_run.check_version()
    if ver == True:
        print("[Success] %s is already up-to-date, exiting." % the_run.app_name)
        exit(0)
    elif ver == False:
        print("Updating %s..." % the_run.app_name)
        inst_proc(the_run)
elif var == False:
    print("Installing %s..." % the_run.app_name)
    inst_proc(the_run)
else:
    print "[Error] at var, exiting..."
    exit(1)
    
# sudo python Desktop/inst_flash.py /Users/Shared/.labtech/logs/`date +%m-%d-%Y_%H%M%S`.txt
#