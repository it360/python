#!/usr/bin/env python

import urllib2, os, subprocess, shutil, time, re, sys, getpass
from sys import argv, exit
from distutils.version import LooseVersion

script, log_file = argv

the_user = getpass.getuser()

if the_user != "root":
    print "Please run as root, exiting."
    exit(1)

class install(object):
    def __init__(self):
        self.app_name = "Flash Player"
        self.inst_base = "/Library/PreferencePanes/Flash Player.prefPane"
        self.inst_inf = self.inst_base+"/Contents/Info.plist"
        self.latest_version = "12.0.0.77"
        self.url = "http://fpdownload.macromedia.com/get/flashplayer/pdc/%s/install_flash_player_osx.dmg" % self.latest_version
        # self.url = "http://fpdownload.macromedia.com/get/flashplayer/current/licensing/mac/install_flash_player_12_osx.dmg?PID=3146232"
        self.dmg_path = "/tmp/flash.dmg"
        self.mnt_path = "/Volumes/Flash Player"
        self.pkg = '/Volumes/Flash Player/Install Adobe Flash Player.app/Contents/Resources/Adobe Flash Player.pkg'
    
    def check_inst(self):
        return os.path.isdir(self.inst_base)
    
    def convert_plist(self, fmt, path):
        cmd = ["plutil", "-convert", fmt, path]
        subprocess.call(cmd)
    
    def check_version(self):
        self.convert_plist("xml1", self.inst_inf)
        a = open(self.inst_inf, "r")
        lines = a.readlines()
        r = re.compile(r'CFBundleVersion')
        for i in range(len(lines)):
            if r.search(lines[i]):
                c = lines[max(0, i+1)]
        d = c.split('<string>')
        e = ''.join(d[1])
        f = e.split('</string>')
        g = f[0]
        self.convert_plist("binary1", self.inst_inf)
        return LooseVersion(g) >= LooseVersion(self.latest_version), g
    
    def download(self):
        f = urllib2.urlopen(self.url) # open the url
        local_file = open(self.dmg_path, "w") # open the file to write to
        local_file.write(f.read()) # write the download to the local file
        local_file.close() # close the file
        a = os.path.isfile(self.dmg_path)
        if a == True:
            return "%s was downloaded.\n" % self.app_name
        elif a == False:
            print "Something went wrong and %s wasn't downloaded, exiting." % self.app_name
            exit(1)
        else:
            print "[Error] at download function, exiting."
            exit(1)
    
    def mount(self):
        cmd = ['/usr/bin/hdiutil', 'attach', '-nobrowse', self.dmg_path]
        subprocess.Popen(cmd, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
        return self.check()
    
    def check(self):
        a = False
        b = 0
        while (a == False) and (b < 60):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print "..."
            b += 1
        if a == False:
            print "Something went wrong and %s still hasn't mounted." % self.app_name
            exit(1)
        elif a == True:
            return "%s was mounted.\n" % self.app_name
        else:
            print "[Error] at mount/check functions, exiting."
            exit(1)
    
    def inst(self):
        cmd = ['/usr/sbin/installer', '-pkg', self.pkg, '-target', '/']
        subprocess.call(cmd)#, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        a = os.path.isdir(self.inst_base)
        if a == True:
            vers = self.check_version()
            if vers[0] == True:
                notice = "%(x)s version %(y)s was installed.\n" % {"x" : self.app_name, "y" : vers[1]}
            elif vers[0] == False:
                notice = "%s is installed but is still not up-to-date. current version: %s" % self.app_name, vers
            else:
                print "[Error] at install success version function/statement, exiting."
                print a
                print vers
                exit(1)
            return notice, vers
        elif a == False:
            return "[Error] %s wasn't installed, exiting." % self.app_name
            exit(1)
        else:
            print "[Error] at install function, exiting."
            exit(1)
    
    def unmount(self):
        cmd = ['/usr/bin/hdiutil', 'detach', '-force', self.mnt_path]
        subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        a = os.path.isdir(self.mnt_path)
        b = 0
        while (a == True) and (b < 30):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print "..."
            b += 1
        if a == False:
            return "Unmounted %s.\n" % self.app_name
        elif a == True:
            print  "[Error] couldn't unmount %s, exiting." % self.app_name
            exit(1)
        else:
            print "[Error] at unmount function, exiting."
            exit(1)
    
    def clean(self):
        os.unlink(self.dmg_path)
        aa = os.path.isfile(self.dmg_path)
        bb = 0
        while (aa == True) and (bb < 30):
            time.sleep(1)
            aa = os.path.isdir(self.mnt_path)
            print "..."
            bb += 1
        if bb == False:
            y = "%s dmg has been deleted.\n" % self.app_name
            return y
        elif bb == True:
            return "[Error] couldn't delete the %s DMG, exiting." % self.app_name
            exit(1)
        else:
            return "[Error] at clean function, exiting."
            exit(1)
            
    def verify(self):
        # check if flash is installed
        verify_inst = os.path.isdir(self.inst_base)
        # check what version
        verify_vers = self.check_version()
        # check that the dmg is unmounted
        verify_unmount = os.path.isdir(self.mnt_path)
        if verify_unmount == True:
            verify_unmount = False
        elif verify_unmount == False:
            verify_unmount = True
        else:
            print "[Error] at verify_unmount, exiting."
            exit(1)
        # check that the dmg was deleted
        verify_rmdmg = os.path.isfile(self.dmg_path)
        if verify_rmdmg == True:
            verify_rmdmg = False
        elif verify_rmdmg == False:
            verify_rmdmg = True
        else:
            print "[Error] at verify_rmdmg, exiting."
            exit(1)
        # report
        verified = verify_inst, verify_vers, verify_unmount, verify_rmdmg
        if False in verified:
            a = "[Error] Something couldn't complete:\n"
            b = "Install: %s\n" % verify_inst
            c = "Updated: %(x)s %(y)s\n" % {"x":verify_vers[0], "y":verify_vers[1]}
            d = "Unmounted: %s\n" % verify_unmount
            e = "Removed DMG: %s\n" % verify_rmdmg
            return a, b, c, d, e
        elif False not in verified:
            a = "[Success], %s has been installed.\n" % self.app_name
            b = "Install: %s\n" % verify_inst
            c = "Updated: %s\n" % verify_vers[0]
            d = "Unmounted: %s\n" % verify_unmount
            e = "Removed DMG: %s\n" % verify_rmdmg
            return a, b, c, d, e
        else:
            print "[Error] at verification return, exiting."
            exit(1)
        
    def the_log(self, log_file, log_out):
        for i in log_out:
            print i
        a = open(log_file, "a")
        for i in log_out:
            a.write(i)
        a.close()
        print "Local log file found at: %s" % log_file



the_run = install()
var = the_run.check_inst()
if var == True:
    # check version
    ver, g = the_run.check_version()
    if ver == True:
        print "%s is already up-to-date, exiting." % the_run.app_name
        print g
    elif ver == False:
        print "need to update"
        print g+" > "+the_run.latest_version
        print the_run.download()
        print the_run.mount()
        print the_run.inst()
        print the_run.unmount()
        print the_run.clean()
        log_out = the_run.verify()
        the_run.the_log(log_file, log_out)
elif var == False:
    print "%s is not installed, exiting" % the_run.app_name
    exit(0)
else:
    print "?"
    
# sudo python Desktop/inst_flash.py /Users/Shared/.labtech/logs/`date +%m-%d-%Y_%H%M%S`.txt