#!/usr/bin/env python
import urllib2, subprocess, os, getpass

running_user = getpass.getuser()
if running_user != "root":
    print("\nPlease run as root, exiting...\n")
    exit(0)

class install(object):
    def __init__(self):
        self.url = "https://github.com/munki/munki/releases/download/v2.4.0/munkitools-2.4.0.2561.pkg"
        self.pkg_file = "/private/tmp/munkitools-2.4.0.2561.pkg"
        self.xml_file = "/private/tmp/munki_deselection.xml"
        self.cmd_string = "sudo installer -applyChoiceChangesXML %s -pkg %s -target /"\
        % (self.xml_file, self.pkg_file)
        self.cmd = self.cmd_string.split(' ')
        
    def download(self):
        network_file = urllib2.urlopen(self.url)
        local_file = open(self.pkg_file, 'w')
        local_file.write(network_file.read())
        local_file.close()
        network_file.close()
    
    def pkg_selection(self, selection_xml):
        xml_file_data = open(self.xml_file, 'w')
        xml_file_data.write(selection_xml)
        xml_file_data.close()
    
    def install(self):
        subprocess.call(self.cmd)
    
    def cleanup(self):
        os.unlink(self.pkg_file)
        os.unlink(self.xml_file)
    
    def launchd(self):
        la = '/Library/LaunchAgents/'
        ld = '/Library/LaunchDaemons/'
        
        ManagedSoftwareCenter =\
        la + 'com.googlecode.munki.ManagedSoftwareCenter.plist'
        managedsoftwareupdate_loginwindow =\
        la + 'com.googlecode.munki.managedsoftwareupdate-loginwindow.plist'
        MunkiStatus =\
        la + 'com.googlecode.munki.MunkiStatus.plist'
        
        logouthelper =\
        ld + 'com.googlecode.munki.logouthelper.plist'
        managedsoftwareupdate_check =\
        ld + 'com.googlecode.munki.managedsoftwareupdate-check.plist'
        managedsoftwareupdate_install =\
        ld + 'com.googlecode.munki.managedsoftwareupdate-install.plist'
        managedsoftwareupdate_manualcheck =\
        ld + 'com.googlecode.munki.managedsoftwareupdate-manualcheck.plist'
        
        agents = [ManagedSoftwareCenter, managedsoftwareupdate_loginwindow,\
        MunkiStatus, logouthelper, managedsoftwareupdate_check,\
        managedsoftwareupdate_install, managedsoftwareupdate_manualcheck]
        
        for _ in agents:
            cmd = ["launchctl", "load", _]
            subprocess.call(cmd)
    
    def config(self, repo_url):
        # deprecating this function to put it into it's own script
        
        SoftwareRepoURL_string = 'defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL %s'\
        % (repo_url)
        SoftwareRepoURL_cmd = SoftwareRepoURL_string.split(' ')
        manifest_cmd = 'defaults write /Library/Preferences/ManagedInstalls ClientIdentifier main'.\
        split(' ')
        apple_updates_cmd = 'defaults write /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates True'.\
        split(' ')
        disable_asus = 'softwareupdate --schedule off'.split(' ')
        
        subprocess.call(SoftwareRepoURL_cmd)
        subprocess.call(manifest_cmd)
        subprocess.call(apple_updates_cmd)
        subprocess.call(disable_asus)

def install_check():
    installed = os.path.isdir\
    ('/Applications/Managed Software Center.app')
    configured = os.path.isfile\
    ('/Library/Preferences/ManagedInstalls.plist')
    
    if installed == True:
        if configured == True:
            should_i_update(True)
    
    return()
                
def should_i_update(installed):
    print("running!")
    info =\
    '/Applications/Managed Software Center.app/Contents/Info.plist'
    update = True
    
    file = open(info, 'r')
    data = file.readlines()
    file.close
    
    for _ in data:
        if '2211' in _:
            update = False
    
    if update == True:
        print("\nMunki needs updating, running the installer...\n")
        return
    elif update != True:
        print("\nMunki is installed and up-to-date, exiting...\n")
        exit(0)

selection_xml = """<array>
	<string>admin</string>
</array>"""

install_check()
install = install()
install.download()
install.pkg_selection(selection_xml)
install.install()
install.cleanup()
#install.config(repo_url)
install.launchd()
