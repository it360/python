#!/usr/bin/env python

import urllib2, os, subprocess, shutil, time, re, getpass
from sys import argv, exit
from distutils.version import LooseVersion

script, log_file = argv

the_user = getpass.getuser()
if the_user != "root":
    print "[Exiting], please run as root."
    exit(1)

class install(object):
    def __init__(self):
        self.app_name = "TeamViewer QuickSupport"
        self.the_app = self.app_name + ".app"
        self.info_file = "/Applications/%s/Contents/Info.plist" % self.the_app
        self.latest_vers = "9.0.25963"
        self.url = "http://download.teamviewer.com/download/TeamViewerQS.dmg"
        self.dmg_path = "/tmp/TeamViewerQS.dmg"
        self.mnt_cmd = ['/usr/bin/hdiutil', 'attach', '-nobrowse', self.dmg_path]
        self.src = "/Volumes/TeamViewerQS/TeamViewerQS.app"
        self.dst = "/Applications/TeamViewerQS.app"
        self.mnt_path = "/Volumes/TeamViewerQS/"
        self.unmnt_cmd = ['/usr/bin/hdiutil', 'detach', self.mnt_path]
    
    def check_inst(self):
        return os.path.isdir(self.dst)
    
    def check_version(self):
        a = open(self.info_file, "r")
        lines = a.readlines()
        r = re.compile(r'CFBundleShortVersionString')
        for i in range(len(lines)):
            if r.search(lines[i]):
                c = lines[max(0, i+1)]
        d = c.split('<string>')
        e = ''.join(d[1])
        f = e.split('</string>')
        g = f[0]
        return LooseVersion(g) >= LooseVersion(self.latest_vers), g
    
    def download(self):
        print "Downloading %s..." % self.app_name
        f = urllib2.urlopen(self.url) # open the url
        local_file = open(self.dmg_path, "w") # open the file to write to
        local_file.write(f.read()) # write the download to the local file
        local_file.close() # close the file
        a = os.path.isfile(self.dmg_path)
        if a == True:
            print "%s was downloaded." % self.app_name
            return "%s was downloaded.\n" % self.app_name
        elif a == False:
            print "[Error], %s couldn't be downloaded, exiting." % self.app_name
            exit(1)
        else:
            print "[Error], exiting."
            exit(1)
        
    def mount(self):
        print "Mounting %s..." % self.app_name
        subprocess.Popen(self.mnt_cmd, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
        return self.check()
    
    def check(self):
        a = False
        b = 0
        while (a == False) and (b < 60):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print "..."
            b += 1
        if a == False:
            print "[Error], The %s DMG couldn't be mounted, exiting" % self.app_name
            exit(1)
        elif a == True:
            print "%s was mounted." % self.app_name
            return "%s was mounted.\n" % self.app_name
        else:
            print "[Error], exiting."
            exit(1)
    
    def copy(self):
        print "Installing %s..." % self.app_name
        shutil.copytree(self.src, self.dst)
        a = os.path.isdir(self.dst)
        if a == True:
            print "%s was installed." % self.app_name
            return "%s was installed.\n" % self.app_name
        elif a == False:
            return "[Error], %s wasn't installed." % self.app_name
        else:
            print "[Error], exiting."
            exit(1)
        
    def cleanup(self):
        print "Unmounting %s..." % self.app_name
        subprocess.Popen(self.unmnt_cmd, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
        a = os.path.isdir(self.mnt_path)
        b = 0
        while (a == True) and (b < 30):
            time.sleep(1)
            a = os.path.isdir(self.mnt_path)
            print "..."
            b += 1
        if a == False:
            print "%s was unmounted." % self.app_name
            z = "Unmounted %s.\n" % self.app_name
        elif a == True:
            return "Couldn't unmount %s." % self.app_name
        else:
            print "[Error], exiting."
            exit(1)
        print "Deleting %s DMG..." % self.app_name
        os.unlink(self.dmg_path)
        aa = os.path.isfile(self.dmg_path)
        bb = 0
        while (aa == True) and (bb < 30):
            time.sleep(1)
            aa = os.path.isdir(self.mnt_path)
            print "..."
            bb += 1
        if bb == False:
            print "%s DMG was deleted." % self.app_name
            print "[Success], %s has been installed." % self.app_name
            y = "[Success], %s DMG was deleted." % self.app_name
        elif bb == True:
            return "[Error], couldn't delete the %s DMG." % self.app_name
        else:
            print "[Error], exiting."
            exit(1)
        return z, y
    
    def the_log(self, log_file, g):
        a = open(log_file, "a")
        for i in g:
            a.write(i)
        a.close()

a = install()
cc = a.check_inst()
if cc == True:
    aa, bb = a.check_version()
elif cc == False:
    b = a.download()
    c = a.mount()
    d = a.copy()
    e, f = a.cleanup()
    g = [b, c, d, e, f]
    for h in g:
        if "[Error]" not in h:
            i = "[Success], %s was installed." % a.app_name
            j = [b, c, d, e, f, i]
        else:
            i = "[Error], something didn't complete properly. Please check the logs."
            j = [b, c, d, e, f, i]
    a.the_log(log_file, j)
    exit(0)
else:
    a.the_log(log_file, "error!")
    exit(1)

if aa == True:
    g = "[Success], up-to-date\n%s\n" % bb
    a.the_log(log_file, g)
    print "[Success], %(z)s is up-to-date (%(y)s)" % {'z':a.app_name, 'y':bb}
    exit(0)
elif aa == False:
    b = a.download()
    c = a.mount()
    d = a.copy()
    e, f = a.cleanup()
    g = [b, c, d, e, f]
    for h in g:
        if "[Error]" not in h:
            i = "[Success], %s was installed." % a.app_name
            j = [b, c, d, e, f, i]
        else:
            i = "[Error], something didn't complete properly. Please check the logs."
            j = [b, c, d, e, f, i]
    a.the_log(log_file, j)
    exit(0)
else:
    a.the_log(log_file, "error!")
    exit(1)