# version feed url http://www.panic.com/updates/transmit/transmit-en.xml

#!/usr/bin/env python

import urllib, urllib2, os, time, re, getpass, subprocess, shutil, time
from sys import argv, exit
from distutils.version import LooseVersion

script, log_file = argv

the_user = getpass.getuser()
if the_user != "root":
    print "[Exiting], please run as root."
    exit(1)

class install(object):
    def __init__(self):
        self.app_name = "FontExplorer X Pro"
        self.the_app = self.app_name + ".app"
        self.info_file = "/Applications/%s/Contents/Info.plist" % self.the_app
        self.url = "https://sendit.it360.biz/link/MDCPA1ddtce2jbL9leUshx/download/FontExplorer-X-Pro.zip"
        #self.vers_url = "http://www.macupdate.com/app/mac/6771/transmit"
        self.latest_vers = "4.2"
        self.zip_path = "/tmp/FontExplorer.zip"
        self.dst = "/Applications/"
        self.unzip_cmd = ['unzip', self.zip_path, '-d', '/Applications/']
        self.app_path = self.dst+self.the_app
        self.osx_folder = self.dst+"__MACOSX"
    
    def check_inst(self):
        return os.path.isdir(self.app_path)
    
    def check_version(self):
        a = open(self.info_file, "r")
        lines = a.readlines()
        r = re.compile(r'CFBundleShortVersionString')
        for i in range(len(lines)):
            if r.search(lines[i]):
                c = lines[max(0, i+1)]
        d = c.split('<string>')
        e = ''.join(d[1])
        f = e.split('</string>')
        g = f[0]
        return LooseVersion(g) >= LooseVersion(self.latest_vers), g
    
    def download(self):
        print "Downloading %s..." % self.app_name
        f = urllib2.urlopen(self.url) # open the url
        local_file = open(self.zip_path, "w") # open the file to write to
        local_file.write(f.read()) # write the download to the local file
        local_file.close() # close the file
        a = os.path.isfile(self.zip_path)
        if a == True:
            print "%s was downloaded." % self.app_name
            return "%s was downloaded." % self.app_name
        elif a == False:
            print "[Error], %s couldn't be downloaded, exiting." % self.app_name
            exit(1)
        else:
            print "[Error], exiting."
            exit(1)
        
    def unzip(self):
        print "Unzipping and installing %s" % self.app_name
        with open(os.devnull, "w") as f:
            subprocess.call(self.unzip_cmd, stdout=f)
        installed = os.path.isdir(self.app_path)
        if installed == True:
            print("%s was installed." % self.app_name)
            return("%s was installed." % self.app_name)
        elif installed == False:
            return("[Error] %s wasn't installed." % self.app_name)
        else:
            print("[Error] at unzip.")
            return("[Error] at unzip.")
        
    def cleanup(self):
        print("Deleting %s zip file..." % self.app_name)
        os.unlink(self.zip_path)
        shutil.rmtree(self.osx_folder)
        file = os.path.isfile(self.zip_path)
        runs = 0
        while (file == True) and (runs < 30):
            time.sleep(1)
            file = os.path.isdir(self.zip_path)
            print("...")
            runs += 1
        if file == False:
            print("%s zip file was deleted." % self.app_name)
            return("%s zip file was deleted." % self.app_name)
        elif bb == True:
            print("[Error] couldn't delete the %s zip file." % self.app_name)
            return("[Error] couldn't delete the %s zip file." % self.app_name)
        else:
            print("[Error] while trying to delete the %s zip file." % self.app_name)
            return("[Error] while trying to delete the %s zip file." % self.app_name)
    
    def update_app(self):
        the_date = time.strftime("%Y-%m-%d_%H%M%S")
        print("Moving the old %s version to /tmp") % self.app_name
        shutil.move(self.app_path, "/tmp/%s_%s" % (self.the_app, the_date))
        print("The old version of %s has been moved.") % self.app_name
        return("The old version of %s has been moved.") % self.app_name
    
    def the_log(self, log_file, g):
        a = open(log_file, "a")
        for i in g:
            a.write(i + '\n')
        a.close()

def install_process(a):
    download_results = a.download()
    unzip_results = a.unzip()
    cleanup_results = a.cleanup()
    log_list = [download_results, unzip_results, cleanup_results]
    return(log_list)
    
def update_process(a):
    updating = "%s is out of date, updating..." % a.app_name
    print(updating)
    log = a.update_app()
    log_list = install_process(a)
    log_list.insert(0, updating)
    log_list.insert(1, log)
    installed = a.check_inst()
    if installed == True:
        updated, version = a.check_version()
        if updated == True:
            return(log_list)
        else:
            error = "[Error] %s wasn't updated." % a.app_name
            log_list.append(error)
            return(log_list)
    else:
        error = "[Error] %s isn't installed now." % a.app_name
        log_list.append(error)
        return(log_list)
    
def logging(a, log_file, log_list):
    for _ in log_list:
        if "[Error]" not in _:
            the_success = "[Success] %s was installed or updated." % a.app_name
        else:
            the_success = "[Error] something didn't complete properly. Please check the logs."
            break
    print(the_success)
    log_list.append(the_success)
    a.the_log(log_file, log_list)

a = install()
cc = a.check_inst()
if cc == True: # installed, check if it's the latest version
    aa, bb = a.check_version()
elif cc == False: # not installed, install
    log = install_process(a)
    logging(a, log_file, log)
    exit(0)
else:
    a.the_log(log_file, "error!")
    print("error checking the install, exiting.")
    exit(1)

if aa == True: # installed and up-to-date
    g = "[Success] up-to-date%s" % bb
    a.the_log(log_file, g)
    print("[Success] %(z)s is up-to-date (%(y)s)" % {'z':a.app_name, 'y':bb})
    exit(0)
elif aa == False: # installed but not up-to-date, update!
    log = update_process(a)
    logging(a, log_file, log)
    exit(0)
else:
    a.the_log(log_file, "error!")
    print("error checking the application version, exiting.")
    exit(1)
    
# change update portion to be a command instead so that I can run this on an entire location and it won't install on non-designer machines