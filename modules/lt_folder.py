#!/usr/bin/env python
import os, subprocess
from sys import exit

launch_agent = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>script_runner</string>
    <key>ProgramArguments</key>
    <array>
		<string>/Users/Shared/.labtech/scripts/script_runner.sh</string>
    </array>
	<key>WatchPaths</key>
	<array>
		<string>/Users/Shared/.labtech/auto_run</string>
	</array>
    <key>RunAtLoad</key>
    <true/>
</dict>
</plist>"""

script_runner = """#!/bin/bash

a="/Users/Shared/.labtech/auto_run"
b=$(ls $a)
the_date=$(date +%m-%d-%Y_%H%M)

file_type ()
{
	if [[ $i == *.sh ]]
		then
		sh $a/$i
	elif [[ $i == *.py ]]
		then
		python $a/$i
	elif [[ $i == *.scpt ]]
		then
		osascript $a/$i
	else
		exit 1
	fi
}

for i in $b
do
	file_type
	sleep 1
	rm -r $a/$i
done"""

def get_user():
    cmd = ['stat', '-f', '\'%Su\'', '/dev/console']
    raw_user = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = raw_user.stdout.read().split('\n')[0]
    user = user.replace("\'", "")
    
    if user != "root":
        return(user)
    else:
        task = subprocess.Popen(['w'], stdout=subprocess.PIPE)
        output = task.stdout.readlines()
        
        for line in output:
            if "console" in line:
                return(line.split(' ')[0])

# labtech directory structure
def lt_dir():
    lt = os.path.isdir("/Users/Shared/.labtech")
    apps = os.path.isdir("/Users/Shared/.labtech/apps")
    logs = os.path.isdir("/Users/Shared/.labtech/logs")
    scripts = os.path.isdir("/Users/Shared/.labtech/scripts")
    receipts = os.path.isdir("/Users/Shared/.labtech/receipts")
    bin = os.path.isdir("/Users/Shared/.labtech/bin")
    auto_run = os.path.isdir("/Users/Shared/.labtech/auto_run")
    a = ".labtech folder exists."
    b = ".labtech/apps folder exists"
    c = ".labtech/logs folder exists"
    d = ".labtech/scripts folder exists"
    e = ".labtech/receipts folder exists"
    f = ".labtech/bin folder exists"
    g = ".labtech/auto_run folder exists"
    if lt == False:
        os.mkdir('/Users/Shared/.labtech')
        a = "Created .labtech folder."
    if apps == False:
        os.mkdir('/Users/Shared/.labtech/apps')
        b = "Created .labtech/apps folder."
    if logs == False:
        os.mkdir('/Users/Shared/.labtech/logs')
        c = "Created .labtech/logs folder."
    if scripts == False:
        os.mkdir('/Users/Shared/.labtech/scripts')
        d = "Created .labtech/scripts folder."
    if receipts == False:
        os.mkdir('/Users/Shared/.labtech/receipts')
        e = "Created .labtech/receipts folder."
    if bin == False:
        os.mkdir('/Users/Shared/.labtech/bin')
        f = "Created .labtech/bin folder."
    if auto_run == False:
        os.mkdir('/Users/Shared/.labtech/auto_run')
        g = "Created .labtech/auto_run folder"
    return a, b, c, d, e, f, g

def lt_dir_permissions(user):
    cmd = ['chown', '-R', user+':staff', '/Users/Shared/.labtech']
    subprocess.call(cmd)

# script_runner
def la_scripts(user, launch_agent):
    la_folder = "/Users/{0}/Library/LaunchAgents".format(user)
    la = os.path.isfile("{0}/com.it360.script_runner.plist".format(la_folder))
    
    if la == False:
        la_folder_exists = os.path.isdir(la_folder)
        
        if la_folder_exists == False:
            os.makedirs(la_folder)
        
        file = open("{0}/com.it360.script_runner.plist".format(la_folder), 'w')
        file.write(launch_agent)
        file.close
        print("Installed the script_runner LaunchAgent.")
        print("The machine will need to be rebooted before the LaunchAgent will work.")
    elif la == True:
        print("The script_runner LaunchAgent is installed.")

def script_runner_install(script_runner):
    script_runner_path = "/Users/Shared/.labtech/scripts/script_runner.sh"
    script_runner_exists = os.path.isfile(script_runner_path)
    
    if script_runner_exists == True:
        print("script_runner is installed.")
    elif script_runner_exists == False:
        cmd = ["chmod", "+x", script_runner_path]
        file = open(script_runner_path, "w")
        file.write(script_runner)
        file.close()
        subprocess.call(cmd)
        print("Installed script_runner.sh")

user = get_user()
results = lt_dir()
lt_dir_permissions(user)
la_scripts(user, launch_agent)
script_runner_install(script_runner)

for _ in results:
    print _
