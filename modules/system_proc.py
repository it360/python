#!/usr/bin/env python

import subprocess, time, os

date_stamp = time.strftime("%Y-%m-%d_%H%M%S")
log_path = '/Users/Shared/.labtech/logs/system_proc/'
file_name = 'ps_%s.txt' % date_stamp
exist_log_path = os.path.isdir(log_path)
cmd = "ps axwww -r -o user,pid,%cpu,%mem,command"
cmd = cmd.split(' ')

if exist_log_path == False:
    os.makedirs(log_path)

b = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
c = b.stdout.read()

d = open(log_path+'/'+file_name, "w")
for e in c:
    d.write(e)
d.close()

# dump ps to a variable
# write the output to a dated txt file
# create a launch agent to run the script