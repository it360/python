#!/usr/bin/env python
import os, subprocess
from sys import exit

def the_user():
    cmd = 'stat -f "%Su" /dev/console'.split(" ")
    result = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    readable_result = result.stdout.read().split("\"")
    return(readable_result[1])

def la_scripts(user, launch_agent):
    la_folder = "/Users/%s/Library/LaunchAgents" % user
    la = os.path.isfile("%s/com.it360.script_runner.plist" % la_folder)
    
    if la == False:
        la_folder_exists = os.path.isdir(la_folder)
        
        if la_folder_exists == False:
            os.mkdir(la_folder)
        
        file = open("%s/com.it360.script_runner.plist" % la_folder, 'w')
        file.write(launch_agent)
        file.close
        print("Installed the script_runner LaunchAgent.")
        print("The machine will need to be rebooted before the LaunchAgent will work.")
    elif la == True:
        print("The script_runner LaunchAgent is installed.")
        
    #return(os.path.isfile("%s/com.it360.script_runner.plist" % la_folder))

def script_runner_install(script_runner):
    script_runner_path = "/Users/Shared/.labtech/scripts/script_runner.sh"
    script_runner_exists = os.path.isfile(script_runner_path)
    
    if script_runner_exists == True:
        print("script_runner is installed.")
    elif script_runner_exists == False:
        cmd = ["chmod", "+x", script_runner_path]
        file = open(script_runner_path, "w")
        file.write(script_runner)
        file.close()
        subprocess.call(cmd)
        print("Installed script_runner.sh")
    
    #return(os.path.isfile(script_runner_path))
        

launch_agent = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>script_runner</string>
    <key>ProgramArguments</key>
    <array>
		<string>/Users/Shared/.labtech/scripts/script_runner.sh</string>
    </array>
	<key>WatchPaths</key>
	<array>
		<string>/Users/Shared/.labtech/auto_run</string>
	</array>
    <key>RunAtLoad</key>
    <true/>
</dict>
</plist>"""

script_runner = """#!/bin/bash

a="/Users/Shared/.labtech/auto_run"
b=$(ls $a)
the_date=$(date +%m-%d-%Y_%H%M)

file_type ()
{
	if [[ $i == *.sh ]]
		then
		sh $a/$i
	elif [[ $i == *.py ]]
		then
		python $a/$i
	elif [[ $i == *.scpt ]]
		then
		osascript $a/$i
	else
		exit 1
	fi
}

for i in $b
do
	file_type
	sleep 1
	rm -r $a/$i
done"""
    
user = the_user()
la_scripts(user, launch_agent)
script_runner_install(script_runner)
