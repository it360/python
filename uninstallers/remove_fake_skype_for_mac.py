#!/usr/bin/env python
import subprocess, os, shutil, glob, getpass
from time import sleep

# Remove Fake Skype For Mac # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                             #
# This script will attempt to remove all of the junk that the 'fake skype'    #
# for mac installs. This is a typical thrown together sysadmin script so use  #
# at your own risk.                                                           #
#                                                                             #
# A warning:                                                                  #
# This script will close and delete Chrome, Firefox, and Safari's preferences #
#                                                                             #
# # # # # # # # # # # # # # # # # # # # # # # # ## # # # # # # # # # # # # #  #


script_owner = getpass.getuser()
if script_owner != "root":
    print("Please run as root, exiting.")
    exit(0)
    
def cfprefsd():
    ps_cmd = "ps aux".split()

    ps_output = subprocess.Popen(ps_cmd, stdout=subprocess.PIPE)
    ps = ps_output.stdout.readlines()

    for _ in ps:
        if "cfprefsd agent" in _:
            user_cfprefsd = _.strip().split()[1]
            
    kill_cmd = ['kill', user_cfprefsd]
    subprocess.call(kill_cmd)

def get_user():
    cmd = "stat -f '%Su' /dev/console".split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = output.stdout.read().strip().replace("'", "")
    return(user)

def remove_zipcloud(user):
    app_path = '/Applications/ZipCloud.app'
    user_cache = '/Users/%s/Library/Caches/COM.ITNT.DM' % user
    user_la = glob.glob('/Users/%s/Library/LaunchAgents/*ZipCloud*' % user)
    boot_cache = glob.glob\
    ('/private/var/db/BootCaches/*/app.com.jdibackup.ZipCloud.playlist')
    receipts = glob.glob('/private/var/db/receipts/com.*.ZipCloud.*')
    
    if os.path.isdir(app_path) == True:
        shutil.rmtree(app_path)
    if os.path.isdir(user_cache) == True:
        shutil.rmtree(user_cache)
    if boot_cache:
        for cache in boot_cache:
            try:
                os.unlink(cache)
            except OSError:
                print("Couldn't remove %s" % cache)
    if user_la:
        for _ in user_la:
            os.unlink(_)
    if receipts:
        for receipt in receipts:
            os.unlink(receipt)

def remove_genieo(user):
    user_genieo_prefs = glob.glob\
    ('/Users/%s/Library/Preferences/*.genieo.*.plist' % user)
    user_genieo_cache = glob.glob('/Users/%s/Library/Caches/*genieo*' % user)
    user_genieo_appsupp = glob.glob\
    ('/Users/%s/Library/Application Support/*genieo*' % user)
    user_genieo_la = glob.glob('/Users/%s/Library/LaunchAgents/*genieo*' % user)
    
    if user_genieo_prefs:
        for _ in user_genieo_prefs:
            os.unlink(_)
    if user_genieo_cache:
        for _ in user_genieo_cache:
            shutil.rmtree(_)
    if user_genieo_appsupp:
        for _ in user_genieo_appsupp:
            shutil.rmtree(_)
    if user_genieo_la:
        for _ in user_genieo_la:
            os.unlink(_)
    
def remove_other(user):
    user_im = glob.glob('/Users/%s/Library/Preferences/*IM.partner*' % user)
    user_mc = glob.glob('/Users/%s/Library/Preferences/*installmc*' % user)
    user_cache = glob.glob('/Users/%s/Library/Caches/*yourcompany*' % user)
    user_skype_dmgs = glob.glob('/Users/%s/Downloads/*kype*' % user)
    
    if user_im:
        for _ in user_im:
            os.unlink(_)
    if user_mc:
        for _ in user_mc:
            os.unlink(_)
    if user_cache:
        for _ in user_cache:
            shutil.rmtree(_)
    if user_skype_dmgs:
        for _ in user_skype_dmgs:
            os.unlink(_)
    
def omnibar(user):
    home_library = "/Users/%s/Library/" % user
    safari_cmd_string = "defaults write /Users/%s/Library/Preferences/com.apple.Safari HomePage http://apple.com/startpage/" % user
    safari_cmd = safari_cmd_string.split(' ')
    safari_cmd_fix_permission_string = "chown %s:staff /Users/%s/Library/Preferences/com.apple.Safari.plist" % (user, user)
    safari_cmd_permission = safari_cmd_fix_permission_string.split(' ')
    safari_cmd_cfpref = "ps aux"
    safari_cmd_kill_safari = "killall Safari".split(' ')
    safari_omnibar = glob.glob(home_library +\
    'Safari/Extensions/*Omnibar*')
    safari_extensions_plist = glob.glob(home_library +\
    'Safari/Extensions/Extensions.plist')
    safari_extension_prefs = glob.glob(home_library +\
    'Preferencese/com.apple.Safari.Extensions.plist')
    chrome_prefs = glob.glob(home_library +\
    'Application Support/Google/Chrome/Default/Preferences')
    chrome_cmd_kill = ["killall", 'Google Chrome']
    firefox_cmd_kill = 'killall firefox'.split(' ')
    firefox_prefs = glob.glob(home_library +\
    'Application Support/Firefox/Profiles/*/prefs.js')
    firefox_search = glob.glob(home_library +\
    'Application Support/Firefox/Profiles/*/search.json')
    firefox_search_plugins = glob.glob(home_library +\
    'Application Support/Firefox/Profiles/*/searchplugins')
    firefox_session = glob.glob(home_library +\
    'Application Support/Firefox/Profiles/*/sessionstore*')
    
    if safari_omnibar:
        subprocess.call(safari_cmd_kill_safari)
        sleep(2)
        for _ in safari_omnibar:
            os.unlink(_)
        for _ in safari_extensions_plist:
            os.unlink(_)
        for _ in safari_extension_prefs:
            os.unlink(_)
        subprocess.call(safari_cmd)
        subprocess.call(safari_cmd_permission)
        
    if chrome_prefs:
        subprocess.call(chrome_cmd_kill)
        sleep(2)
        for _ in chrome_prefs:
            os.unlink(_)
        
    if firefox_prefs:
        subprocess.call(firefox_cmd_kill)
        sleep(2)
        for _ in firefox_prefs:
            os.unlink(_)
        for _ in firefox_session:
            os.unlink(_)
        for _ in firefox_search:
            os.unlink(_)
        for _ in firefox_search_plugins:
            shutil.rmtree(_)
    cfprefsd()
        
def CleanMyMac(user):
    bootcaches = glob.glob('/private/var/db/BootCaches/*/*CleanMyMac*')
    shared = glob.glob('/Users/Shared/*CleanMyMac*')
    app_support = glob.glob('/Users/%s/Library/Application Support/*CleanMyMac*' % user)
    preferences = glob.glob('/Users/%s/Library/Preferences/*CleanMyMac*' % user)
    logs = glob.glob('/Users/%s/Library/Logs/*CleanMyMac*' % user)
    caches = glob.glob('/Users/%s/Library/Caches/*CleanMyMac*' % user)
    
    if bootcaches:
        for _ in bootcaches:
            try:
                shutil.rmtree(_)
            except OSError:
                os.unlink(_)
    if shared:
        for _ in shared:
            try:
                shutil.rmtree(_)
            except OSError:
                os.unlink(_)
    if app_support:
        for _ in app_support:
            try:
                shutil.rmtree(_)
            except OSError:
                os.unlink(_)
    if preferences:
        for _ in preferences:
            try:
                shutil.rmtree(_)
            except OSError:
                os.unlink(_)
    if logs:
        for _ in logs:
            try:
                shutil.rmtree(_)
            except OSError:
                os.unlink(_)
    if caches:
        for _ in caches:
            try:
                shutil.rmtree(_)
            except OSError:
                os.unlink(_)

user = get_user()
remove_zipcloud(user)
remove_genieo(user)
remove_other(user)
omnibar(user)
CleanMyMac(user)