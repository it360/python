#!/usr/bin/env python

import glob, getpass, os, shutil

the_user = getpass.getuser()
if the_user != "root":
    print "[Exiting], please run as root."
    exit(1)

app_path = '/Applications/TeamViewer.app'
old_app_path = glob.glob('/Applications/*/TeamViewer.app')
launchd_paths = glob.glob('/Library/LaunchAgents/com.teamviewer.*')
appsupport_path = glob.glob('/Users/*/Library/Application Support/TeamViewer')
font_path = '/Library/Fonts/TeamViewer9.otf'


try:
    shutil.rmtree(app_path)
except:
    print("")
for _ in old_app_path:
    shutil.rmtree(_)
for _ in launchd_paths:
    os.unlink(_)
for _ in appsupport_path:
    shutil.rmtree(_)
try:
    os.unlink(font_path)
except:
    print("")

# /Applications/TeamViewer.app
# /Library/LaunchAgents/com.teamviewer.teamviewer_desktop.plist
# /Library/LaunchAgents/com.teamviewer.teamviewer.plist
# /Users/bryan/Library/Application Support/TeamViewer
# /Library/Fonts/TeamViewer9.otf