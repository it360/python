#!/usr/bin/env python

import subprocess, getpass
from sys import argv

running_user = getpass.getuser()
if running_user != "root":
    print("\nPlease run as root, exiting...\n")
    exit(0)

def get_console_user():
    cmd = 'stat -f "%Su" /dev/console'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = output.stdout.read().strip()
    return(user.replace('"', ''))

def add_user(user):
    file = open('/etc/sudoers', 'r')
    text = file.readlines()
    file.close()
    
    for _ in text:
        if user in _:
            print("User is already in the sudoers file, returning...")
            return

    text.append('%s\tALL=(ALL) ALL' % user)

    file_write = open('/etc/sudoers', 'w')
    for _ in text:
        file_write.write(_)
    file_write.close()
    
    print("Added %s to the sudoers file." % user)
    
def remove_user(user):
    file = open('/etc/sudoers', 'r')
    text = file.readlines()
    file.close()
    
    for _ in text:
        if user in _:
            rm_line = _
    if rm_line:
        text.remove(rm_line)
    
    file_w = open('/etc/sudoers', 'w')
    for _ in text:
        file_w.write(_)
    file_w.close()
    
    print("Removed %s to the sudoers file." % user)

def help():
    print("\nPlease run with either of the following switches:")
    print("--add\t\tAdds the user to the sudoers file")
    print("--remove\tRemoves the user from the sudoers file\n")
    exit(1)
    
try:
    script, arg = argv
except ValueError:
    help()

user = get_console_user()
if arg == "--add":
    add_user(user)
elif arg == "--remove":
    remove_user(user)
else:
    help()