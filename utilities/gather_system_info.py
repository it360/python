#!/usr/bin/env python

import subprocess, urllib2, re
from AppKit import NSPasteboard, NSArray
from sys import argv

try:
    script, location = argv
except ValueError:
    print("\nNo path specified, the data will be copied to the clipboard.\n\n")

def get_serial():
    cmd = ["system_profiler", "SPHardwareDataType"]
    raw_data = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    serial_data = raw_data.stdout.readlines()
    for _ in serial_data:
        if "Serial Number" in _:
            serial = _.split(' ')[-1].strip()
    return(serial)
    

def get_model(serial):
    serial_part = serial[-4:]
    url = "http://support-sp.apple.com/sp/product?cc=%s" % serial_part
    page = urllib2.urlopen(url)
    data = page.read()
    
    raw_model = re.compile(r'<configCode>(.*?)</configCode>').search(data)
    model = raw_model.group(1)
    return(model)
    
def get_model_id():
    cmd = ["system_profiler", "SPHardwareDataType"]
    raw_hardware = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    hardware_data = raw_hardware.stdout.readlines()
    for _ in hardware_data:
        if "Model Identifier" in _:
            model = _
    return(model.strip().split(' ')[-1])
    
def get_mac_address():
    cmd = ["networksetup", "-listallhardwareports"]
    raw_report = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    report_blob = raw_report.stdout.read()
    report = report_blob.split('\n\n')
    interfaces = []
    mac_address = []
    for _ in report:
        if "en" in _:
            lines = _.split('\n')
            interface = lines[0].split(':')[-1].strip()
            MAC = lines[-1].split(' ')[-1].strip()
            interfaces.append(interface + " " + MAC)
    for _ in interfaces:
        if "Ethernet" in _ or "Wi-Fi" in _ or "AirPort" in _:
            mac_address.append(_)
    return(mac_address)
    
def pasteboard(content):
    pb = NSPasteboard.generalPasteboard()
    pb.clearContents()
    content_array = NSArray.arrayWithObject_(content)
    pb.writeObjects_(content_array)
    
serial = get_serial()
model_name = get_model(serial)
model_id = get_model_id()
mac_address = get_mac_address()

string = model_name + "\t" + model_id + "\t" + serial + "\t" +\
    '\t'.join(mac_address)

try:
    if location:
        file = open(location, 'w')
        file.write(string)
        file.close()
        print("\nThe following has been saved to %s\n" % location)
        print(string + "\n")
except NameError:
    pasteboard(string)
    print("The following has been copied to your clipboard:\n")
    print(string + "\n")