#!/usr/bin/python
import os
import glob
import logging
import subprocess
from sys import argv
from datetime import date, timedelta

def loggums():
    global log_path
    global log_file
    global debug_file
    
    if os.path.isdir(log_path) != True:
        os.makedirs(log_path)
    
    logging.basicConfig(filename=log_file, level=logging.INFO, format='%(asctime)s - %(levelname)s -  %(message)s', datefmt='%Y-%m-%d %I:%M:%S %p')
    
    if os.path.isfile(debug_file):
        logging.getLogger().setLevel(logging.DEBUG)
    
    logging.info("*** Starting tm_manager ***")
            
def termy(cmd, desc):
    task = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = task.communicate()
    
    if out:
        logging.info("Subprocess: {0}\n{1}".format(desc, out))
    if err:
        logging.error("Subprocess: {0}\n{1}".format(desc, err))

def debugger():
    global debug_file
    
    if os.path.isfile(debug_file):
        file = open(debug_file, 'r')
        ip = file.read()
        file.close()
    else:
        return
    
    log_level = logging.getLogger().getEffectiveLevel()
    if log_level > 10:
        return
    print("Running the debugger...")
    logging.info("Running the debugger...")
    
    import socket
    
    # get the current ip
    print("Getting the current IP...")
    ip_address = socket.gethostbyname(socket.gethostname())
    print(ip_address)
    logging.debug("Current IP: {0}".format(ip_address))
    
    # get the current dns
    print("\nGetting the current DNS...")
    dns = []
    resolve_conf = open('/etc/resolv.conf', 'r')
    dns_file = resolve_conf.readlines()
    resolve_conf.close()
    for line in dns_file:
        if "nameserver" in line:
            dns.append(line.split(' ')[-1].strip())
    dns = ''.join(dns)
    print(dns)
    logging.debug("Current DNS: {0}".format(dns))
    
    # ping test to the tm synology
    print("\nTesting the connection to the Synology...")
    cmd = ['/sbin/ping', '-c', '4', ip]
    task = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = task.communicate()
    print(out)
    logging.debug("Ping results: {0}".format(out))
    
    # port test 548 to the tm synology
    print("\nTesting the Synology AFP port...")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    port_status = sock.connect_ex((ip, 548))
    if port_status == 0:
        print("Port is open.")
        logging.debug("AFP port is open.")
    else:
        print("Port is closed.")
        logging.debug("AFP port is closed.")

def tm_list():
    cmd = ["tmutil", "listbackups"]
    raw_backup_list = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    backup_blob = raw_backup_list.stdout.read()
    backup_list = filter(None, backup_blob.split('\n'))
    tm_remove_list(backup_list)
    
    log = "List of backups: {0}".format('\n'.join(backup_list))
    logging.debug(log)

def tm_remove_list(backup_list):
    remove_list = []
    for _ in backup_list:
        year, month, day = _.split('/')[-1].split('-')[0:3]
        remove = date.today() - date(int(year), int(month), int(day)) > timedelta(days = 30)
        if remove == True:
            remove_list.append(_)
    
    if remove_list == []:
        log = "List of backups to remove: None"
    else:
        log = "List of backups to remove: {0}".format('\n'.join(remove_list))
    logging.info("Number of backups to remove: {0}".format(len(remove_list)))
    logging.debug(log)
    
    tm_remove(remove_list)

def tm_remove(remove_list):
    for _ in remove_list:
        rm_cmd = ["tmutil", "delete", _]
        task = subprocess.Popen(rm_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = task.communicate()
        
        log  = "Removing: {0}\nOutput: {1}\nErrors: {2}".format(_, out, err)
        logging.debug(log)

def tm_eject():
    global tm_backups
    tm_share = glob.glob('/Volumes/TimeMachine*')[0]
    
    if os.path.exists(tm_backups):
        cmd = ['hdiutil', 'unmount', tm_backups]
        kill_store_cmd = ['/usr/bin/killall', 'mds_stores']
        kill_mds_cmd = ['/usr/bin/killall', 'mds']
        termy(kill_store_cmd, "Killing the mds_stores process")
        termy(kill_mds_cmd, "Killing the mds process")
        termy(cmd, "Unmounting the TimeMachine backup")
    if os.path.exists(tm_share):
        cmd = ['hdiutil', 'eject', '-force', tm_share]
        termy(cmd, "Ejecting the TimeMachine volume")
        
def enable_debug(ip):
    global debug_path
    global debug_file
    
    if os.path.isdir(debug_path) != True:
        os.makedirs(debug_path)
        
    file = open(debug_file, 'w')
    file.write(ip)
    file.close()
    
def disable_debug():
    global debug_file
    os.unlink(debug_file)

def switch(item):
    global args
    index = args.index(item) + 1
    return(args[index])
        
def helper():
    print("*** tm_manager help ***")
    print("--debug [on|off]")
    print("\tEnables|Disables debug mode.")
    print("\t[on|off] On to enable debugging, off to disable debugging.")
    print("--ip [ip]")
    print("\t[ip] Is the IP address to test with.")
    exit()



debug_path = '/usr/local/it360/receipts/'
debug_file = os.path.join(debug_path, 'tmm.debug')
log_path = '/usr/local/it360/logs/'
log_file = os.path.join(log_path, 'tm_manager.log')
tm_backups = '/Volumes/Time Machine Backups'


try:
    script, args = argv
    if '-h' in args:
        helper()
    if '--debug' in args:
        debug_switch = switch('--debug')
        if debug_switch == 'on':
            if '--ip' in args:
                ip = switch('--ip')
            else:
                helper()
        elif debug_switch == 'off':
            disable_debug
        else:
            helper()
except ValueError:
    print('running tm')
    loggums()
    debugger()
    tm_list()
    tm_eject()


# add functions to log the machines current state
# # IP, ping test, port test
