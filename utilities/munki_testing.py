#!/usr/bin/python
# uses glob to cheaply find pkginfo files and check if they're set to the testing catalog
# and prints each file that is in testing
import os
import glob

pkginfo = '/Users/Shared/munki_repo/pkgsinfo/*/*'

pkginfo_files = []
pkginfo_glob = glob.glob(pkginfo)

for _ in pkginfo_glob:
    if os.path.isfile(_):
        pkginfo_files.append(_)

for _ in pkginfo_files:
    file = open(_)
    data = file.readlines()
    file.close()
    for line in data:
        if "testing" in line:
            print(_)
