#!/usr/bin/env python
import os, subprocess
from sys import argv
from time import strftime

def get_console_user():
    cmd = 'stat -f "%Su" /dev/console'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = output.stdout.read().strip()
    return(user.replace('"', ''))
    
def fix_permissions(user, license_path):
    cmd =\
    ['chown', '-R', user + ':staff', license_path + '/OmniGraffle.omnilicense']
    subprocess.call(cmd)
    
user = get_console_user()
script, owner, license = argv
date_added = strftime("%m/%d/%Y %H:%M")
license_path =\
'/Users/%s/Library/Application Support/Omni Group/Software Licenses/'\
% user

license_contents = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>AddedByUser</key>
	<string>%s</string>
	<key>Application</key>
	<string>1000207</string>
	<key>DateAdded</key>
	<string>%s America/Chicago</string>
	<key>Key</key>
	<string>%s</string>
	<key>Owner</key>
	<string>%s</string>
</dict>
</plist>""" % (user, date_added, license, owner)


try:
    os.makedirs(license_path)
except OSError:
    print("\nSoftware Licenses folder already exists.\n")

file = open(license_path + 'OmniGraffle.omnilicense', 'w')
file.write(license_contents)
file.close()
if os.path.isfile(license_path + 'OmniGraffle.omnilicense') == True:
    print("\nThe OmniGraffle license has been installed.\n")
else:
    print("\nError, OmniGraffle license still isn't installed.\n")

fix_permissions(user, license_path)





# License file is found in
# /Users/$USER/Library/Application Support/Omni Group/Software Licenses/

# License file scheme OmniGraffle.omnilicense

# License file contents: (see license_contents above)

# Need variables for AddedByUser, DateAdded, License Key, Owner

# Because this will run as root but needs to be placed into the $USERS folder
# I'll need to fix the permissions on the file once it's created.