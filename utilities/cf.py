#!/usr/bin/python
"""Create, Update, and Delete CloudFlare DNS entries"""
import json
import httplib
from pprint import pprint

email = '' # cloudflare@example.com
key = '' # https://www.cloudflare.com/a/account/my-account found at the bottom of my settings
zone = '' # example.com

            
def new_dns_record(dns, new_ip):
    """Creates a new DNS A record"""
    api_cmd = 'rec_new'
    dns_type = 'A'
    ttl = '1'
    fmt = "a={0}&tkn={1}&email={2}&z={3}&type={4}&name={5}&content={6}&ttl={7}".format(api_cmd, key, email, zone, dns_type, dns, new_ip, ttl)
    pprint(api_call(fmt))

def update_dns_record(dns, new_ip):
    """Updates the specified DNS A record's IP address"""
    dns_id = get_dns_id(dns)
    api_cmd = 'rec_edit'
    dns_type = 'A'
    ttl = '1'
    service_mode = '0'
    fmt = "a={0}&tkn={1}&id={2}&email={3}&z={4}&type={5}&name={6}&content={7}&ttl={8}&service_mode={9}".format(api_cmd, key, dns_id, email, zone, dns_type, dns, new_ip, ttl, service_mode)
    pprint(api_call(fmt))

def delete_dns_record(dns):
    """Deletes the specified DNS record"""
    dns_id = get_dns_id(dns)
    api_cmd = 'rec_delete'
    fmt = "a={0}&tkn={1}&email={2}&z={3}&id={4}".format(api_cmd, key, email, zone, dns_id)
    pprint(api_call(fmt))
    
def check_and_update(dns):
    """Checks your current IP against what CloudFlare
has and updates it if necessary"""
    external_ip = get_external_ip()
    cf_ip = get_dns_record(dns)['content']
    if external_ip != cf_ip:
        print("Updating IP to {0}".format(external_ip))
        update_dns_record(dns, '{0}'.format(external_ip))
    else:
        print("IP doesn't need updating.")



def api_call(api_cmd):
    """Makes the API requests and returns JSON data"""
    req = httplib.HTTPSConnection('www.cloudflare.com')
    req.request('GET', '/api_json.html?{0}'.format(api_cmd))
    response = req.getresponse()
    data = response.read().decode('utf-8')
    return(json.loads(data))

def get_dns_records():
    """Returns a JSON list of all DNS entries for the specified zone"""
    api_cmd = 'rec_load_all'
    dns_records = api_call("a={0}&email={1}&tkn={2}&z={3}".format(api_cmd, email, key, zone))
    dns_list = dns_records['response']['recs']['objs'] # list of DNS records
    return(dns_list)
    
def get_dns_record(dns):
    """Returns a specific DNS records JSON data"""
    dns_list = get_dns_records()
    for _ in dns_list:
        if dns in _['name']:
            dns_index = dns_list.index(_)
            return(dns_list[dns_index])
            
def get_dns_id(dns):
    """Returns the specified DNS records ID"""
    dns_list = get_dns_records()
    for _ in dns_list:
        if dns in _['name']:
            dns_index = dns_list.index(_)
            break
    dns_id = dns_list[dns_index]['rec_id']
    return(dns_id)
    
def get_external_ip():
    """Checks ifconfig.co to get your external IP"""
    req = httplib.HTTPConnection('ifconfig.co')
    req.request('GET', '/json')
    response = req.getresponse()
    data = response.read().decode('utf-8')
    return(json.loads(data)['ip'])



#new_dns_record('tmp', '0.0.0.0')
#update_dns_record('test', '0.0.0.0')
#delete_dns_record('test')
#check_and_update('test')


# https://www.cloudflare.com/docs/client-api.html
#   a: api service
#       rec_load_all (get all DNS)
#       rec_edit (edit DNS record)
#       rec_new (add new DNS record)
#       rec_delete (delete DNS record)
#   service_mode: 1 = orange cloud, 0 = grey cloud
#   ttl: 1 = Automatic, otherwise, value must in between 120 and 86400 seconds
#   content: DNS record content (example: A == 0.0.0.0)
#   name: name of the DNS record (example: test.example.com)
#   id: rec_id from the DNS record JSON data
#   type: A/CNAME/MX/TXT/SPF/AAAA/NS/SRV/LOC
#   z: target domain (example: example.com)
