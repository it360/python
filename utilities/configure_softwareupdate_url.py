#!/usr/bin/env python
import getpass, subprocess, platform
from sys import exit
from sys import argv

the_user = getpass.getuser()
if the_user != "root":
    print "[Exiting], please run as root."
    exit(1)
    
script, options, base_url = argv
  
osx = platform.mac_ver()[0]
osx_version = '.'.join(osx.split('.')[0:2])

class software_updates(object):
    def __init__(self, base_url):
        self.temp_var = 4
        self.cmd_add = ["defaults", "write", "/Library/Preferences/com.apple.SoftwareUpdate", "CatalogURL", self.temp_var]
        self.cmd_remove = ["defaults", "delete", "/Library/Preferences/com.apple.SoftwareUpdate", "CatalogURL"]

        branch = "main"
        # main branch urls
        self.tiger_main = "http://%s/content/catalogs/index_main.sucatalog" % base_url
        self.leopard_main = "http://%s/content/catalogs/others/index-leopard.merged-1_main.sucatalog" % base_url
        self.snow_leopard_main = "http://%s/content/catalogs/others/index-leopard-snowleopard.merged-1_main.sucatalog" % base_url
        self.lion_main = "http://%s/content/catalogs/others/index-lion-snowleopard-leopard.merged-1_main.sucatalog" % base_url
        self.mountain_lion_main = "http://%s/content/catalogs/others/index-mountainlion-lion-snowleopard-leopard.merged-1_main.sucatalog" % base_url
        self.mavericks_main = "http://%s/content/catalogs/others/index-10.9-mountainlion-lion-snowleopard-leopard.merged-1_main.sucatalog" % base_url
        self.yosemite_main = "http://%s/content/catalogs/others/index-10.10-10.9-mountainlion-lion-snowleopard-leopard.merged-1_main.sucatalog" % base_url
        self.elcapitan_main = "http://%s/content/catalogs/others/index-10.11-10.10-10.9-mountainlion-lion-snowleopard-leopard.merged-1_main.sucatalog" % base_url
        # testing branch urls
        self.tiger_testing = "http://%s/content/catalogs/index_testing.sucatalog" % base_url
        self.leopard_testing = "http://%s/content/catalogs/others/index-leopard.merged-1_testing.sucatalog" % base_url
        self.snow_leopard_testing = "http://%s/content/catalogs/others/index-leopard-snowleopard.merged-1_testing.sucatalog" % base_url
        self.lion_testing = "http://%s/content/catalogs/others/index-lion-snowleopard-leopard.merged-1_testing.sucatalog" % base_url
        self.mountain_lion_testing = "http://%s/content/catalogs/others/index-mountainlion-lion-snowleopard-leopard.merged-1_testing.sucatalog" % base_url
        self.mavericks_testing = "http://%s/content/catalogs/others/index-10.9-mountainlion-lion-snowleopard-leopard.merged-1_testing.sucatalog" % base_url
        self.yosemite_testing = "http://%s/content/catalogs/others/index-10.10-10.9-mountainlion-lion-snowleopard-leopard.merged-1_testing.sucatalog" % base_url
        self.elcapitan_testing = "http://%s/content/catalogs/others/index-10.11-10.10-10.9-mountainlion-lion-snowleopard-leopard.merged-1_testing.sucatalog" % base_url
        
    def software_update_url(self, temp_var):
        self.cmd_add[4] = temp_var
        subprocess.call(self.cmd_add)
    def software_update_remove_url(self):
        subprocess.call(self.cmd_remove)
            
    def main(self, osx_version, branch):
        if osx_version == "10.4":
            if branch == "testing":
                self.software_update_url(self.tiger_testing)
            else:
                self.software_update_url(self.tiger_main)
        elif osx_version == "10.5":
            if branch == "testing":
                self.software_update_url(self.leopard_testing)
            else:
                self.software_update_url(self.leopard_main)
        elif osx_version == "10.6":
            if branch == "testing":
                self.software_update_url(self.snow_leopard_testing)
            else:
                self.software_update_url(self.snow_leopard_main)
        elif osx_version == "10.7":
            if branch == "testing":
                self.software_update_url(self.lion_testing)
            else:
                self.software_update_url(self.lion_main)
        elif osx_version == "10.8":
            if branch == "testing":
                self.software_update_url(self.mountain_lion_testing)
            else:
                self.software_update_url(self.mountain_lion_main)
        elif osx_version == "10.9":
            if branch == "testing":
                self.software_update_url(self.mavericks_testing)
            else:
                self.software_update_url(self.mavericks_main)
        elif osx_version == "10.10":
            if branch == "testing":
                self.software_update_url(self.yosemite_testing)
            else:
                self.software_update_url(self.yosemite_main)
        elif osx_version == "10.11":
            if branch == "testing":
                self.software_update_url(self.elcapitan_testing)
            else:
                self.software_update_url(self.elcapitan_main)
        else:
            print("error!")

if options == "--testing":
    run = software_updates(base_url)
    run.main(osx_version, "testing")
elif options == "--main":
    run = software_updates(base_url)
    run.main(osx_version, "main")
elif options == "--remove":
    run = software_updates(base_url)
    run.software_update_remove_url()
else:
    print("error")
