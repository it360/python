#!/usr/bin/env python
from subprocess import call
from time import strftime
from sys import argv

try:
    script, switch = argv
except ValueError:
    switch = "0"

def start():
    cmd = "vboxmanage startvm ownCloud".split(' ')
    call(cmd)
    
def stop():
    cmd = "vboxmanage controlvm ownCloud poweroff".split(' ')
    call(cmd)
    
def snapshot():
    ss_date = strftime("%Y-%m-%d_%H-%M-%S")
    cmd = ["vboxmanage", "snapshot", "ownCloud", "take", ss_date]
    call(cmd)
    
def help():
    print("Commands:")
    print("\t--start starts the VM")
    print("\t--stop stops the VM")
    print("\t--snapshot runs a snapshot")
    print("\t--help or -h prints a list of commands")
    
if switch == "--start":
    print("Starting the ownCloud VM")
    start()
elif switch == "--stop":
    print("Stopping the ownCloud VM")
    stop()
elif switch == "--snapshot":
    print("Taking a snapshot of the ownCloud VM")
    snapshot()
elif switch == "--help":
    help()
elif switch == "-h":
    help()
else:
    help()