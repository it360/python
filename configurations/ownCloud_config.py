#!/usr/bin/env python
import os, subprocess, pwd

def main(user):
    path = '/Users/%s/Library/Application Support/ownCloud/folders' % user
    ownPath = os.path.exists(path)
    if ownPath == False:
        os.makedirs(path)
    own_cfg(cfg, user)
    create_configs(ownCloud, user)
    fix_permissions(user)

def get_user():
    cmd = 'stat -f "%Su" /dev/console'.split(' ')
    raw_user = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    return(raw_user.stdout.read().rstrip().replace('\"', ''))

def own_cfg(cfg, user):
    file_path = '/Users/%s/Library/Application Support/ownCloud/owncloud.cfg' % user
    file = open(file_path, 'w')
    file.write(cfg)
    file.close()

def create_configs(ownCloud, user):
    file_path = '/Users/%s/Library/Application Support/ownCloud/folders/ownCloud' % user
    file = open(file_path, 'w')
    file.write(ownCloud)
    file.close()
    
def fix_permissions(user):
    cmd = ["chown", "-R", user+":staff", "/Users/"+user+"/Library/Application Support/ownCloud/"]
    subprocess.call(cmd)
    
    
user = get_user()

cfg = """[General]
optionalDesktopNotifications=true
monoIcons=true

[ownCloud]
url=http://own.danwaibel.com/
authType=http
user=%s
http_user=%s

[Proxy]
type=2""" % (user, user)

ownCloud = """[ownCloud]
localPath=/Users/%s/Documents/ownCloud
targetPath=/
backend=owncloud
connection=ownCloud""" % user

main(user)