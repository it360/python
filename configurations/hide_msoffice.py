#!/usr/bin/env python
import subprocess

def user():
    cmd = 'stat -f "%Su" /dev/console'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = output.stdout.read().strip()
    return(user.replace('"', ''))
    
def hide(path):
    cmd = ['chflags', 'hidden', path]
    subprocess.call(cmd)
    
path = '/Users/%s/Documents/Microsoft User Data' % user()
hide(path)