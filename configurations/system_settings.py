#!/usr/bin/env python
import subprocess, getpass

user = getpass.getuser()
if user != "root":
    print("Please run as root, exiting.")
    exit(0)
    
# functions #
# intel gathering #
def check_model_info():
    cmd = 'system_profiler SPHardwareDataType'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    model_line = output.stdout.readlines()
    for _ in model_line:
        if "Model Name" in _:
            model_name_string = _.split(':')[-1]
        if "Model Identifier" in _:
            model_string = _.strip().split(' ')[-1]
    model_name = model_name_string.replace(' ', '').strip()
    model_number = model_string.replace(model_name, '').split(',')[0]
    retina = False
    if "MacBookPro" in model_name:
        if int(model_number) < 10:
            retina = False
        else:
            retina = True
    return(model_name)

def battery(model_name):
    if model_name == "MacBook":
        battery = True
    elif model_name == "MacBookPro":
        battery = True
    elif model_name == "MacBookAir":
        battery = True
    elif model_name == "iMac":
        battery = False
    elif model_name == "Macmini":
        battery = False
    elif model_name == "MacPro":
        battery = False
    elif model_name == "Mac":
        battery = False
    return(battery)
    
def get_sleep():
    cmd = 'pmset -g'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    data = output.stdout.readlines()
    for _ in data:
        if "sleep" in _[0:6]:
            sleep = filter(None, _.strip().split(' '))[1]
    if int(sleep) == 1:
        return(False)
    else:
        return(True)

# configuring settings #
def fast_user_switching():
    print("Enabling fast user switching...\n")
    cmd = "defaults write /Library/Preferences/.GlobalPreferences MultipleSessionEnabled -bool YES".\
    split(' ')
    subprocess.call(cmd)
    
def energy_settings(model_name, battery, change_sleep):
    print("Setting energy saver settings...\n")
    bat_disp_sleep = 'pmset -b displaysleep 15'.split(' ')
    char_disp_sleep = 'pmset -c displaysleep 30'.split(' ')
    bat_sleep = 'pmset -b sleep 30'.split(' ')
    char_sleep = 'pmset -c sleep 60'.split(' ')
    disable_reduce_brightness = 'pmset -b lessbright 0'.split(' ')
    
    if battery == True:
        subprocess.call(bat_disp_sleep)
        subprocess.call(char_disp_sleep)
        subprocess.call(disable_reduce_brightness)
        if change_sleep == True:
            # it's ok to change sleep settings
            subprocess.call(bat_sleep)
            subprocess.call(char_sleep)
    else:
        # not a laptop
        subprocess.call(char_disp_sleep)
        if change_sleep == True:
            # it's ok to change sleep settings
            subprocess.call(char_sleep)
            
def autologin():
    print("Disabling auto login...\n")
    read_cmd = "defaults read /Library/Preferences/com.apple.loginwindow.plist".\
    split(' ')
    write_cmd =\
    "defaults delete /Library/Preferences/com.apple.loginwindow autoLoginUser"\
    .split(' ')
    
    output = subprocess.Popen(read_cmd, stdout=subprocess.PIPE)
    defaults_data = output.stdout.readlines()
    
    for _ in defaults_data:
        if "autoLoginUser" in _:
            subprocess.call(write_cmd)

def name_and_password():
    print("Setting require name and password for login...")
    cmd = "defaults write /Library/Preferences/com.apple.loginwindow.plist SHOWFULLNAME 1".\
    split()
    subprocess.call(cmd)

def ssh(): # sets only the administrators group to access
    print("Enabling SSH...")
    enable_ssh_cmd = "systemsetup -setremotelogin on".split(' ')
    ssh_access_cmd = 'dseditgroup -o edit -a admin -t group com.apple.access_ssh'.\
    split(' ')
    
    subprocess.call(enable_ssh_cmd)
    subprocess.call(ssh_access_cmd)

def ard():
    print("Enabling ARD...")
    cmd = "sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -access -on -restart -agent -configure -allowAccessFor -allUsers -privs -all".\
    split(' ')
    subprocess.call(cmd)
    
def enable_root():
    username = raw_input("User to enable root: ")
    password = getpass.getpass("User's password: ")
    newpass = getpass.getpass("Root's new password: ")
    newpass2 = getpass.getpass("Repeat Root's new password: ")
    
    loop = True
    
    while loop == True:
        if newpass != newpass2:
            print("\nThe new password doesn't match, please try again.\n")
            newpass = getpass.getpass("Root's new password: ")
            newpass2 = getpass.getpass("Repeat Root's new password: ")
        if newpass == newpass2:
            loop = False
            break
            
    cmd = ['dsenableroot', '-u', username, '-p', password, '-r', newpass]
    subprocess.call(cmd)


# function calls #
# intel functions #
model_name = check_model_info()
battery = battery(model_name)
change_sleep = get_sleep()

# settings functions
fast_user_switching()
autologin()
name_and_password()
ssh()
ard()
energy_settings(model_name, battery, change_sleep)
enable_root()