#!/usr/bin/env python
import os
import glob
import json
import time
import subprocess

class main(object):
    def __init__(self):
        self.install_check()
        self.get_console_user()
        self.find_workflow()
        self.convert('json')
        self.read_file()
        self.manipulate_data()
        self.write_file()
        self.convert('xml1')
        self.receipt()
        
    def get_console_user(self):
        cmd = 'stat -f "%Su" /dev/console'.split(' ')
        output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        user = output.stdout.read().strip()
        self.user = user.replace('"', '')
    
    def find_workflow(self):
        app_path = '/Applications/Connect to Simantel Servers.app'
        home_path = '/Users/%s/Applications/Connect to Simantel Servers.app' % self.user
        if os.path.exists(app_path):
            self.file = os.path.join(app_path, 'Contents/document.wflow')
        elif os.path.exists(home_path):
            self.file = os.path.join(home_path, 'Contents/document.wflow')
        else:
            print("Cannot find the path, exiting...")
            exit()
    
    def convert(self, fmt):
        cmd = ['plutil', '-convert', fmt, self.file]
        subprocess.call(cmd)
        
    def read_file(self):
        file = open(self.file, 'r')
        self.data = json.load(file)
        file.close()
        
    def manipulate_data(self):
        self.connections = self.data['actions'][0]['action']['ActionParameters']['items']
        for connection in self.connections:
            if "Archive" in connection['name']:
                connection_index = self.connections.index(connection)
        move_connection = self.connections.pop(connection_index)
        self.connections.append(move_connection)
        for connection in self.connections:
            print(connection)
        
    def write_file(self):
        with open(self.file, 'w') as file:
            file.write(json.dumps(self.data, file, indent=4))
            
    def install_check(self):
        file_path = '/Users/Shared/.labtech/receipts/move_archive_CSS.json'
        if os.path.exists(file_path):
            print("Connect to Simantel Servers has already been updated, exiting...")
            exit()
            
    def receipt(self):
        file_path = '/Users/Shared/.labtech/receipts/move_archive_CSS.json'
        date_stamp = time.strftime('%Y-%m-%d')
        with open(file_path, 'w') as file:
            file.write(json.dumps({'installed':date_stamp}, file, indent=4))
            
main()