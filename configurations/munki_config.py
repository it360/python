#!/usr/bin/env python

import subprocess, getpass
from sys import argv

try:
    script, repo_url = argv
except ValueError:
    print("\nPlease run again and include the repo URL, exiting...\n")
    exit(0)
    
running_user = getpass.getuser()
if running_user != "root":
    print("\nPlease run as root, exiting...\n")
    exit(0)

def configure(repo_url):
    SoftwareRepoURL_string =\
    'defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL %s'\
    % (repo_url)
    SoftwareRepoURL_cmd = SoftwareRepoURL_string.split(' ')
    manifest_cmd =\
    'defaults write /Library/Preferences/ManagedInstalls ClientIdentifier main'.\
    split(' ')
    #apple_updates_cmd =\
    #'defaults write /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates True'.\
    #split(' ')
    #apple_updates_cmd =\
    #'defaults delete /Library/Preferences/ManagedInstalls InstallAppleSoftwareUpdates'.\
    #split(' ')
    #disable_asus = 'softwareupdate --schedule off'.split(' ')
    
    subprocess.call(SoftwareRepoURL_cmd)
    subprocess.call(manifest_cmd)
    #subprocess.call(apple_updates_cmd)
    #subprocess.call(disable_asus)

configure(repo_url)