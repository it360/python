#!/usr/bin/env python
import os, subprocess, platform, time, pwd, grp, json
from distutils.version import LooseVersion

def get_user():
    cmd = 'stat -f "%Su" /dev/console'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = output.stdout.read().strip()
    return(user.replace('"', ''))
    
def la_folder(la_dir):
    if not os.path.isdir(la_dir):
        os.makedirs(la_dir)
        
def write_la(la_dir, la):
    file = open(la_dir + 'com.it360.messages.plist', 'w')
    file.write(la)
    file.close()
    
def fix_permissions(user, la_dir):
    group_id = pwd.getpwnam(user).pw_gid
    try:
        group = grp.getgrgid(group_id).gr_name
    except KeyError:
        group = group_id
    cmd = ['chown', '-R', '%s:%s' % (user, group), la_dir]
    subprocess.call(cmd)
    
def version_compare():
    os_vers = platform.mac_ver()[0]
    iChat = LooseVersion(os_vers) < LooseVersion('10.8')
    if iChat == True:
        app = 'iChat.app'
    else:
        app = 'Messages.app'
    return(app)
    
def install_check(la_dir):
    plist = la_dir + 'com.it360.messages.plist'
    receipt = '/Users/Shared/.labtech/receipts/deploy_messages_la.json'
    if os.path.isfile(plist) == True:
        if os.path.exists(receipt):
            print("The Messages LaunchAgent is already installed, exiting...")
            exit(0)
            
def receipt():
    receipt = '/Users/Shared/.labtech/receipts/deploy_messages_la.json'
    date_stamp = time.strftime('%Y-%m-%d')
    with open(receipt, 'w') as file:
        file.write(json.dumps({'installed':date_stamp}, file, indent=4))
    

la = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.it360.messages</string>
    <key>ProgramArguments</key>
    <array>
        <string>open</string>
        <string>/Applications/%s</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>LaunchOnlyOnce</key>
    <true/>
</dict>
</plist>""" % version_compare()

user = get_user()
la_dir = '/Users/%s/Library/LaunchAgents/' % user
install_check(la_dir)
la_folder(la_dir)
write_la(la_dir, la)
fix_permissions(user, la_dir)
receipt()