#!/usr/bin/env python

import os
import glob
import getpass
import urllib2
import subprocess

def get_console_user():
    cmd = 'stat -f "%Su" /dev/console'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = output.stdout.read().strip()
    return(user.replace('"', ''))

def install_checks():
    bin_path = "/usr/local/it360/bin/"
    bin = os.path.isdir(bin_path)
    dockutil = '/usr/local/it360/bin/dockutil'
    
    if not os.path.isdir(bin_path):
        print("The bin folder doesn't exist.")
        print("Creating {0} ...\n".format(bin_path))
        try:
            os.makedirs(bin_path)
        except OSError:
            print("Permission denied, please run as root, exiting...")
            exit(1)
        print("Created {0}".format(bin_path))
    if not os.path.isfile(dockutil):
        print("Installing dockutil...")
        dockutil_install(dockutil)
        if os.path.isfile(dockutil):
            print("dockutil has been installed.")
            user = getpass.getuser()
            if user == "root":
                print("Running as root, please run as the user. Exiting...")
                exit(0)
        else:
            print("Error, dockutil wasn't installed, exiting...")
            exit(1)

def dockutil_install(dockutil):
    url = "https://raw.githubusercontent.com/kcrawford/dockutil/master/scripts/dockutil"
    page = urllib2.urlopen(url)
    data = page.read()
    page.close()
    
    try:
        file = open(dockutil, 'w')
        file.write(data)
        file.close()
    except IOError:
        print("Permission denied, please run as root, exiting...")
        exit(1)
    
    perm_cmd = ["chmod", "755", dockutil]
    subprocess.call(perm_cmd)
    exec_cmd = ["chmod", "+x", dockutil]
    subprocess.call(exec_cmd)

def termy(cmd):
    task = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = task.communicate()
    if out:
        print("cmd output: {0}\n\t{1}".format(cmd, out))
    if err:
        print("cmd errors: {0}\n\t{1}".format(cmd, err))
        
def docker(app, pos, loc):
    # takes the position option and where that position is
    global user
    global dockutil
    termy([dockutil, '--add', app, pos, loc, '--homeloc', '/Users/{0}'.format(user), '--no-restart'])

def dock_config():
    global user
    global dockutil
    add_cmd_list = []
    user_vol = os.path.join('/Volumes/', user)
    
    # remove all items from the Dock, star fresh
    termy([dockutil, '--remove', 'all', '--no-restart'])
    
    # built in apps
    siri = '/Applications/Siri.app'
    launchpad = '/Applications/Launchpad.app'
    safari = '/Applications/Safari.app'
    calendar = '/Applications/Calendar.app'
    notes = '/Applications/Notes.app'
    reminders = '/Applications/Reminders.app'
    if os.path.isdir(siri):
        docker(siri, '--position', 'beginning')
        docker(launchpad, '--after', 'Siri')
    else:
        docker(launchpad, '--position', 'beginning')
    docker(safari, '--position', 'end')
    docker(calendar, '--position', 'end')
    docker(notes, '--position', 'end')
    docker(reminders, '--position', 'end')
    
    
    # office suite
    office = '/Applications/Microsoft Office 2011/'
    outlook2011 = os.path.join(office, 'Microsoft Outlook.app')
    word2011 = os.path.join(office, 'Microsoft Word.app')
    excel2011 = os.path.join(office, 'Microsoft Excel.app')
    powerpoint2011 = os.path.join(office, 'Microsoft PowerPoint.app')
    excel2016 = '/Applications/Microsoft Excel.app'
    outlook2016 = '/Applications/Microsoft Outlook.app'
    powerpoint2016 = '/Applications/Microsoft PowerPoint.app'
    word2016 = '/Applications/Microsoft Word.app'
    if os.path.isdir(outlook2016):
        docker(outlook2016, '--position', 'end')
        docker(word2016, '--position', 'end')
        docker(excel2016, '--position', 'end')
        docker(powerpoint2016, '--position', 'end')
    elif os.path.isdir(office):
        docker(outlook2011, '--position', 'end')
        docker(word2011, '--position', 'end')
        docker(excel2011, '--position', 'end')
        docker(powerpoint2011, '--position', 'end')
    
    # adobe apps 
    # if creative; before itunes; PS, ID, AI, FEX
    # if video; before itunes; premier, AE, PS, FEX
    try:
        photoshop = glob.glob('/Applications/Adobe Photoshop CC*/Adobe Photoshop*')[-1]
        docker(photoshop, '--position', 'end')
    except:
        pass
    try:
        indesign = glob.glob('/Applications/Adobe InDesign CC */Adobe InDesign*')[-1]
        docker(indesign, '--position', 'end')
    except:
        pass
    try:
        illustrator = glob.glob('/Applications/Adobe Illustrator CC*/Adobe Illustrator*')[-1]
        docker(illustrator, '--position', 'end')
    except:
        pass
    fex = '/Applications/FontExplorer X Pro.app'
    if os.path.isdir(fex):
        docker(fex, '--position', 'end')
    
    # custom apps, kinda
    messages = '/Applications/Messages.app'
    forticlient = '/Applications/FortiClient.app'
    ctss = '/Users/{0}/Applications/Connect to Simantel Servers.app'.format(user)
    docker(messages, '--position', 'end')
    docker(forticlient, '--position', 'end')
    docker(ctss, '--position', 'end')
    
    # add dock folder + options
    apps_cmd = [dockutil, '--add', '/Applications', '--view', 'list', '--display', 'folder', '--position', 'beginning', '--sort', 'name', '--no-restart']
    adown_cmd = [dockutil, '--add', '/Users/{0}/Downloads'.format(user), '--view', 'grid', '--display', 'folder', '--sort', 'name', '--no-restart']
    user_cmd = [dockutil, '--add', user_vol, '--view', 'grid', '--display', 'folder', '--position', 'end', '--no-restart']
    termy(apps_cmd)
    termy(adown_cmd)
    if os.path.isdir(user_vol):
        termy(user_cmd)
    else:
        print("User share not mounted, skipping.")
    
    # restart the dock
    termy(["killall", "Dock"])
    

dockutil = '/usr/local/it360/bin/dockutil'
user = get_console_user()
install_checks()
dock_config()
