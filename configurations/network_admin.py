#!/usr/bin/env python
import sys
import getpass
import platform
import plistlib
import subprocess
from distutils.version import LooseVersion

class main(object):
    def __init__(self):
        user = getpass.getuser()
        if user != "root":
            print("Please run as root, exiting...")
            exit(0)
            
        osx_vers = platform.mac_ver()[0]
        if LooseVersion(osx_vers) < LooseVersion('10.9'):
            print("This can only be run on 10.9 or newer machine, exiting...")
            exit(0)
            
        self.group = 'everyone'
        self.net_pref = 'system.preferences.network'
        self.net_config_pref = 'system.services.systemconfiguration.network'
        
    def read_auth(self, preference):
        cmd = ['/usr/bin/security', 'authorizationdb', 'read', preference]
        
        task = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = task.communicate()
        
        formatted = plistlib.readPlistFromString(out)
        return(formatted)
        
    def group_check(self):
        """if self.read_auth(self.net_pref)['group'] == 'admin':
            self.group_unlocker(self.net_pref)
        elif self.read_auth(self.net_pref)['group'] == 'everyone':
            print("%s is already set." % self.net_pref)
        else:
            print("Something went wrong, exiting...")"""
            
        if self.read_auth(self.net_config_pref)['group'] == 'admin':
            self.group_unlocker(self.net_config_pref)
        elif self.read_auth(self.net_config_pref)['group'] == 'everyone':
            print("%s is already set." % self.net_config_pref)
        else:
            print("Something went wrong, exiting...")
            
    def group_unlocker(self, preference):
        authdb = self.read_auth(preference)
        authdb['group'] = self.group
        input_authdb = plistlib.writePlistToString(authdb)
        self.write_auth(preference, input_authdb)
        
    def write_auth(self, preference, input_authdb):
        cmd = ['/usr/bin/security', 'authorizationdb', 'write', preference]
        task = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = task.communicate(input=input_authdb)
        print("\nSet the preference group for %s to %s.\n" % (preference, self.group))
        
    def remove_auth(self):
        self.group = 'admin'
        if self.read_auth(self.net_pref)['group'] == 'everyone':
            self.group_unlocker(self.net_pref)
        if self.read_auth(self.net_config_pref)['group'] == 'everyone':
            self.group_unlocker(self.net_config_pref)

run = main()
run.group_check()
#run.remove_auth()

# system.services.networkextension.filtering
# system.services.networkextension.vpn
# system.services.systemconfiguration.network