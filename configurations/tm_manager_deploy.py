#!/usr/bin/env python
import os

if os.path.exists('/Users/Shared/.labtech/scripts/tm_manager.py') == True:
    print('tm_manager is already installed, exiting...')
    exit(0)

tm_manager = """#!/usr/bin/env python
import subprocess
from datetime import date, timedelta

def tm_list():
    cmd = ["tmutil", "listbackups"]
    raw_backup_list = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    backup_blob = raw_backup_list.stdout.read()
    backup_list = filter(None, backup_blob.split('\\n'))
    tm_remove_list(backup_list)

def tm_remove_list(backup_list):
    remove_list = []
    for _ in backup_list:
        year, month, day = _.split('/')[-1].split('-')[0:3]
        remove = date.today() - date(int(year), int(month), int(day)) > timedelta(days = 30)
        if remove == True:
            remove_list.append(_)
    tm_remove(remove_list)

def tm_remove(remove_list):
    for _ in remove_list:
        rm_cmd = ["tmutil", "delete", _]
        subprocess.call(rm_cmd)

tm_list()"""

tm_run = """#!/bin/bash
python /Users/Shared/.labtech/scripts/tm_manager.py
tmutil startbackup"""

if os.path.isdir('/Library/TimeMachine') != True:
    print("/Library/TimeMachine folder doesn't exist, exiting...")
    exit(0)
    
file = open('/Users/Shared/.labtech/scripts/tm_manager.py', 'w')
file.write(tm_manager)
file.close()

file = open('/Library/TimeMachine/run.timemachine.sh', 'w')
file.write(tm_run)
file.close()

# pull out tm_manager, make it into it's own script file
# commit tm_manager
# update this script to download tm_manager and install it
