#!/usr/bin/env python
import subprocess, os, shutil
from random import randint

# functions #
def get_last_user():
    cmd = 'stat -f "%Su" /dev/console'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = output.stdout.read().strip()
    return(user.replace('"', ''))
    
def user_switching_icon(user):
    print("\nSetting user switching icon...\n")
    cmd_string = "defaults write /Users/%s/Library/Preferences/.GlobalPreferences userMenuExtraStyle 2"\
    % user
    cmd = cmd_string.split(' ')
    quit_uiserver_cmd = "killall SystemUIServer".split(' ')
    
    subprocess.call(cmd)
    subprocess.call(quit_uiserver_cmd)

def finder_defaults():
    print("Setting Finder defaults...\n")
    status_bar_cmd = "defaults write com.apple.finder ShowStatusBar 1".split(' ')
    path_bar_cmd = "defaults write com.apple.finder ShowPathbar 1".split(' ')
    tab_bar_cmd = "defaults write com.apple.finder ShowTabView 1".split(' ')
    
    desktop_ext_drives =\
    "defaults write com.apple.finder ShowExternalHardDrivesOnDesktop 1".split(' ')
    desktop_drives =\
    "defaults write com.apple.finder ShowHardDrivesOnDesktop 1".split(' ')
    desktop_media =\
    "defaults write com.apple.finder ShowRemovableMediaOnDesktop 1".split(' ')
    desktop_server =\
    "defaults write com.apple.finder ShowMountedServersOnDesktop 1".split(' ')
    target_home =\
    "defaults write com.apple.finder NewWindowTarget PfHm".split(' ')
    search_current =\
    "defaults write com.apple.finder FXDefaultSearchScope SCcf".split(' ')
    
    battery_percent_cmd =\
    "defaults write com.apple.menuextra.battery ShowPercent YES".split(' ')
    quit_uiserver_cmd = "killall SystemUIServer".split(' ')
    
    cmds = [status_bar_cmd, path_bar_cmd, tab_bar_cmd, desktop_ext_drives,\
    desktop_drives, desktop_media, desktop_server, target_home,\
    search_current, battery_percent_cmd, quit_uiserver_cmd]
    
    for cmd in cmds:
        subprocess.call(cmd)

def screen_saver():
    # having an issue where only flurry is in the system preference pane
    # this causes the other savers to throw an error and doesn't set properly
    print("Setting screen saver...\n")
    style_num = randint(1,3)
    if style_num == 1:
        screen_saver_cmd = """osascript -e 'tell app "System Events" to set current screen saver to screen saver named "Arabesque"'"""
    if style_num == 2:
        screen_saver_cmd = """osascript -e 'tell app "System Events" to set current screen saver to screen saver named "Flurry"'"""
    if style_num == 3:
        screen_saver_cmd = """osascript -e 'tell app "System Events" to set current screen saver to screen saver named "Shell"'"""
    idle_cmd = "defaults -currentHost write com.apple.screensaver idleTime 600"\
    .split()
    subprocess.call(idle_cmd)
    os.system(screen_saver_cmd)

def security():
    print("Setting security settings...\n")
    ask_for_password_cmd =\
    "defaults write com.apple.screensaver askForPassword 1".split()
    ask_for_password_delay_cmd =\
    "defaults write com.apple.screensaver askForPasswordDelay 60".split()
    subprocess.call(ask_for_password_cmd)
    subprocess.call(ask_for_password_delay_cmd)

def keyboard_tab(user):
    print("Setting keyboard settings...\n")
    cmd_string = "defaults write /Users/%s/Library/Preferences/.GlobalPreferences.plist AppleKeyboardUIMode 2" % user
    cmd = cmd_string.split(' ')
    subprocess.call(cmd)

def text(user):
    print("Setting text settings...\n")
    dash = "NSAutomaticDashSubstitutionEnabled"
    quote = "NSAutomaticQuoteSubstitutionEnabled"
    spelling = "NSAutomaticSpellingCorrectionEnabled"
    web_spelling = "WebAutomaticSpellingCorrectionEnabled"
    defaults_list = [dash, quote, spelling, web_spelling]
    for _ in defaults_list:
        cmd = ["defaults", "write", "/Users/%s/Library/Preferences/.GlobalPreferences.plist" % user, _, '0']
        subprocess.call(cmd)

def trackpad(user):
    # note ###############################################################
    # these commands will show that bottom corner right click is enabled #
    # *but* right click will not work                                    #
    ######################################################################
    print("Setting trackpad settings...\n")
    global_cmd =\
    "defaults -currentHost write .GlobalPreferences com.apple.trackpad.trackpadCornerClickBehavior 1".\
    split()
    multitouch_cmd =\
    "defaults -currentHost write com.apple.AppleMultitouchTrackpad.plist TrackpadCornerSecondaryClick 2".\
    split()
    bluetooth_cmd =\
    "defaults -currentHost write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick 2".\
    split()
    scrolling_string =\
    "defaults -currentHost write /Users/%s/Library/Preferences/.GlobalPreferences com.apple.swipescrolldirection -bool false" % user
    scrolling = scrolling_string.split(' ')
    
    subprocess.call(global_cmd)
    subprocess.call(multitouch_cmd)
    subprocess.call(bluetooth_cmd)
    subprocess.call(scrolling)

def safari(user):
    print("Setting Safari defaults...\n")
    user_pref = "/Users/%s/Library/Preferences/com.apple.Safari" % user
    tabbar_cmd = ["defaults", "write", user_pref, "AlwaysShowTabBar", "1"]
    safedl_cmd = ["defaults", "write", user_pref, "AutoOpenSafeDownloads", "0"]
    
    subprocess.call(tabbar_cmd)
    subprocess.call(safedl_cmd)
    #cfprefsd()
    
def rm_downloads(user):
    try:
        print("Deleting About Downloads...\n")
        shutil.rmtree('/Users/%s/Downloads/About Downloads.lpdf' % user)
    except(OSError):
        print("About Downloads already deleted.\n")

def cfprefsd():
    print("Killing cfprefsd")
    cmd = ["killall", "cfprefsd"]
    subprocess.call(cmd)

# function calls #
user = get_last_user()
user_switching_icon(user)
finder_defaults()
screen_saver()
security()
keyboard_tab(user)
text(user)
trackpad(user)
safari(user)
rm_downloads(user)