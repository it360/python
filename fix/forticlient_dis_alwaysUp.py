#!/usr/bin/python
import os
import getpass

vpn_file = "/Library/Application Support/Fortinet/FortiClient/conf/vpn.plist"

running_user = getpass.getuser()
if running_user != "root":
    print("Please run as root, exiting...")
    exit(0)

def disable():
    global vpn_file
    file = open(vpn_file, 'r')
    data = file.readlines()
    file.close()
    index = 0
    for _ in data:
        if "<key>KeepRunning</key>" in _:
            indexd = index+1
            break
        index += 1
    data[indexd] = data[indexd].replace('1', '0')
    file = open(vpn_file, 'w')
    for _ in data:
        file.write(_)
    file.close()

if os.path.isfile(vpn_file):
    disable()
