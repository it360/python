#!/usr/bin/env python
import getpass, os, platform
from sys import exit

if getpass.getuser() != "root":
    print('Please run as root, exiting.')
    exit(1)
    
osx = platform.mac_ver()[0]
osx_version = '.'.join(osx.split('.')[1:2])
if osx_version != "9":
    print("This is configured to only run on OS X Mavericks, exiting.")
    exit(1)

config_exists = os.path.isfile('/etc/nsmb.conf')
if config_exists == True:
    print("The SMB configuration exists, exiting.")
    exit(0)

def smb_config():
    config = """[default]
smb_neg=smb1_only"""
    
    file = open('/etc/nsmb.conf', 'w')
    file.write(config)
    file.close()
    
if config_exists != True:
    if osx_version == "9":
        smb_config()