#!/usr/bin/env python
import subprocess

def get_console_user():
    cmd = 'stat -f "%Su" /dev/console'.split(' ')
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    user = output.stdout.read().strip()
    return(user.replace('"', ''))
    
def defaults_fex(user):
    path = '/Users/%s/Library/Preferences/com.linotype.FontExplorerX.plist' % user
    cmd_activate =\
    ['defaults', 'write', path, 'plugInsActivateAutomatically', '1']
    cmd_deactivate_app =\
    ['defaults', 'write', path, 'plugInsDeactivateOnAppQuit', '1']
    cmd_deactivate_server =\
    ['defaults', 'write', path, 'plugInsDeactivateServerFontsOnAppQuit', '1']
    
    subprocess.call(cmd_activate)
    subprocess.call(cmd_deactivate_app)
    subprocess.call(cmd_deactivate_server)

user = get_console_user()

installed = os.path.isdir\
("/Users/%s/Library/Application Support/Linotype/FontExplorer X/" % user)

if installed == True:
    defaults_fex(user)