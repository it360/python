#!/usr/bin/env python
import subprocess, time, glob, getpass, plistlib
from sys import argv

def sudo_check():
    user = getpass.getuser()
    if user != "root":
        print("Please run as root, exiting...")
        exit(0)

def config_7545(address):
    cmd = ['lpadmin', '-p', 'w7545p', '-v', address, '-D', 'Downstairs W7545P', '-L', 'Downstairs', '-P', '/Library/Printers/PPDs/Contents/Resources/en.lproj/Xerox WorkCentre7500Series(EFI)', '-E']
    cmd_base = 'lpadmin -p w7545p -o printer-is-shared=false -o EFFinisher=Finisher_S03885 -o EFPunchOpt=3Even'
    cmd2 = cmd_base.split(' ')
    a = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    b = subprocess.Popen(cmd2, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def config_560(address):
    cmd = ['lpadmin', '-p', 'xc560', '-v', address, '-D', 'Upstairs XC560', '-L', 'Upstairs', '-P', '/Library/Printers/PPDs/Contents/Resources/en.lproj/Xerox 550-560 Integrated Fiery', '-E']
    cmd_base = 'lpadmin -p xc560 -o printer-is-shared=false -o EFXerox_Advanced_Pro_Finisher=Profeesional_NA'
    cmd2 = cmd_base.split(' ')
    a = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    b = subprocess.Popen(cmd2, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
def config_520(address):
    cmd = ['lpadmin', '-p', 'ld520c', '-v', address, '-D', 'Lanier LD520C', '-L', 'ROOM', '-P', '/Library/Printers/PPDs/Contents/Resources/LANIER MP C2050:LD520C', '-o', 'printer-is-shared=false', '-E']
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def rm_printer(printer):
    cmd = ['lpadmin', '-x', printer]
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
def lpstat():
    cmd = ['lpstat', '-a']
    a = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    printers = a.stdout.readlines()
    for printer in printers:
        count = printers.index(printer)
        printers[count] = printer.split(' ')[0]
    return(printers)
    
def printer_address():
    file = open('/Library/Preferences/org.cups.printers.plist', 'r')
    data = file.read()
    file.close()
    formatted = plistlib.readPlistFromString(data)
    printers = {}
    for _ in formatted:
        printer = _['printer-name']
        uri = _['device-uri']
        if 'xc560' in printer:
            printers['xc560'] = uri
        if 'Lanier' in printer or 'ld520c' in printer:
            printers['ld520c'] = uri
        if 'w7545p' in printer:
            printers['w7545p'] = uri
    return(printers)

sudo_check()

printers = lpstat()
address = printer_address()

if 'xc560' in printers:
    print("\nCycling the xc560 printer...")
    print("\tRemoving the xc560 printer...")
    rm_printer('xc560')
    time.sleep(1)
    print("\tConfiguring the xc560 printer...")
    config_560(address['xc560'])
    time.sleep(1)
    if 'xc560' in lpstat():
        print("\tThe xc560 printer has been re-configured.")
    else:
        print("\t[Error] the xc560 has not be re-configured.")
if 'Xerox_550_560_Integrated_Fiery' in printers:
    print("\nCycling the xc560 printer...")
    print("\tRemoving the xc560 printer...")
    rm_printer('Xerox_550_560_Integrated_Fiery')
    time.sleep(1)
    print("\tConfiguring the xc560 printer...")
    config_560(address['Xerox_550_560_Integrated_Fiery'])
    time.sleep(1)
    if 'xc560' in lpstat():
        print("\tThe xc560 printer has been re-configured.")
    else:
        print("\t[Error] the xc560 has not be re-configured.")
if 'w7545p' in printers:
    print("\nCycling the w7545p printer...")
    print("\tRemoving the w7545p printer...")
    rm_printer('w7545p')
    time.sleep(1)
    print("\tConfiguring the w7545p printer...")
    config_7545(address['w7545p'])
    time.sleep(1)
    if 'w7545p' in lpstat():
        print("\tThe w7545p printer has been re-configured.")
    else:
        print("\t[Error] the w7545p printer has not been re-configured.")
if 'Xerox_WorkCentre7500Series_EFI_' in printers:
    print("\nCycling the w7545p printer...")
    print("\tRemoving the w7545p printer...")
    rm_printer('Xerox_WorkCentre7500Series_EFI_')
    time.sleep(1)
    print("\tConfiguring the w7545p printer...")
    config_7545(address['Xerox_WorkCentre7500Series_EFI_'])
    time.sleep(1)
    if 'w7545p' in lpstat():
        print("\tThe w7545p printer has been re-configured.")
    else:
        print("\t[Error] the w7545p printer has not been re-configured.")
if 'Lanier' in printers:
    print("\nCycling the ld520c printer...")
    print("\tRemoving the ld520c printer...")
    rm_printer('Lanier')
    time.sleep(1)
    print("\tConfiguring the ld520c printer...")
    config_520(address['ld520c'])
    time.sleep(1)
    if 'ld520c' in lpstat():
        print("\tThe ld520c printer has been re-configured.")
    else:
        print("\t[Error] the ld520c printer has not been re-reconfigured.")
if 'ld520c' in printers:
    print("\nCycling the ld520c printer...")
    print("\tRemoving the ld520c printer...")
    rm_printer('ld520c')
    time.sleep(1)
    print("\tConfiguring the ld520c printer...")
    config_520(address['ld520c'])
    time.sleep(1)
    if 'ld520c' in lpstat():
        print("\tThe ld520c printer has been re-configured.")
    else:
        print("\t[Error] the ld520c printer has not been re-configured.")
print("")